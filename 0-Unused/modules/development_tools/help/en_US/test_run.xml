<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="test_run" xml:lang="en">
  <info>
    <pubdate>$LastChangedDate: 2008-03-14 11:58:09 +0100 (ven, 14 mar 2008) $</pubdate>
  </info>
  <refnamediv>
    <refname>test_run</refname>
    <refpurpose>Launch tests</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      N = test_run()
      N = test_run(module[,test_name[,options]])
    </synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>module</term>
        <listitem>
          <para>a vector of string. It can be the name of a module or the absolute path of a toolbox.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>test_name</term>
        <listitem>
          <para>a vector of string</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>options</term>
        <listitem>
          <para>a vector of string</para>
          <itemizedlist>
            <listitem>
              <para>no_check_ref : does not check if the .dia and .dia.ref are equal</para>
            </listitem>
            <listitem>
              <para>no_check_error_output</para>
            </listitem>
            <listitem>
              <para>create_ref : create the .dia.ref file and does not check if the .dia and .dia.ref are equal</para>
            </listitem>
            <listitem>
              <para>list : does not perform the tests but displays a list of available tests</para>
            </listitem>
            <listitem>
              <para>help : display some examples about how to use this command</para>
            </listitem>
            <listitem>
              <para>mode_nw : add the "-nw" option to the launch</para>
            </listitem>
            <listitem>
              <para>mode_nwni : add the "-nwni" option to the launch</para>
            </listitem>
            <listitem>
              <para>nonreg_test : runs only the non-regression tests, skipping unit tests</para>
            </listitem>
            <listitem>
              <para>unit_test : runs only the unit tests, skipping non-regression tests</para>
            </listitem>
            <listitem>
              <para>skip_tests</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
   Search for .tst files in the unit test and non-regression test library
   execute them, and display a report about success of failures.
   The .tst files are searched in directories SCI+"/modules/*/tests/unit_tests"
   and SCI+"/modules/*/tests/nonreg_tests".
   Whenever a test is executed, a .dia file is generated which contains 
   the full list of commands executed along with message which appears in the
   console. When the script is done, the .dia file is compared with 
   the .dia.ref file which is expected to be in the same directory 
   as the .tst file. If the two file are different, the test fails.
   </para>
   <para>
   Special tags may be inserted in the .tst file, which help to 
   control the processing of the corresponding test. These tags
   are expected to be found in Scilab comments.
   </para>
    <para>These are the available tags :</para>
   <itemizedlist>
     <listitem>
       <para>
       &#60;-- INTERACTIVE TEST --&#62;
       This test will be skipped because it is interactive.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- NOT FIXED --&#62;
       This test will be skipped because it is a known, but unfixed bug.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- TEST WITH GRAPHIC --&#62;
       This test will not be executed if the option "mode_nwni" is used.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- NO TRY CATCH --&#62;
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- NO CHECK ERROR OUTPUT --&#62;
       The error output file is not checked
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- NO CHECK REF --&#62;
       The .dia and the .dia.ref files are not compared.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- ENGLISH IMPOSED --&#62;
       This test will be executed with the -l en_US option.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- FRENCH IMPOSED --&#62;
       This test will be executed with the -l fr_FR option.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- JVM NOT MANDATORY --&#62;
       This test will be executed with the nwni mode by default.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- WINDOWS ONLY --&#62;
       If the operating system isn't Windows, the test is skipped.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- UNIX ONLY --&#62;
       If the operating system isn't an unix OS, the test is skipped.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- LINUX ONLY --&#62;
       If the operating system isn't Linux, the test is skipped.
       </para>
       </listitem>
       <listitem>
       <para>
       &#60;-- MACOSX ONLY --&#62;
       If the operating system isn't MacOSX, the test is skipped.
       </para>
       </listitem>
     </itemizedlist>
   
    <para>
   Each test is executed in a separated process, created with the "host" command.
   That enables the current command to continue, even if the test as
   created an unstable environment. It also enables the tests to be 
   independent from one another.
   </para>
  </refsection>

<refsection>
    <title>Platform-specific tests</title>
    <para>
    It may happen that the output of a test depends on the platform on which it is 
    executed. In this case, the <literal>.ref</literal> file cannot be correct for 
    all platforms and unit tests may fail for some platform. In this case, we can 
    create a default <literal>.ref</literal> and create additionnal <literal>.ref</literal> 
    file for each platform. 
   </para>

    <para>
    The various platform-specific <literal>.ref</literal> files must have one of the following extensions.
   </para>
    
   <itemizedlist>
       <listitem>
       <para>
       <literal>.unix.dia.ref</literal> for Unix platform,
       </para>
       </listitem>

       <listitem>
       <para>
       <literal>.linux..diaref</literal> for Linux platform,
       </para>
       </listitem>
       
       <listitem>
       <para>
       <literal>.win.dia.ref</literal> for Windows platform,
       </para>
       </listitem>
       
       <listitem>
       <para>
       <literal>.macosx.dia.ref</literal> for Mac OS X platform.
       </para>
       </listitem>
       
     </itemizedlist>
    
   <para>
   The algorithm is the following. 
   First, the <literal>.ref</literal> is considered. If this file does not exist,
   the platform-specific <literal>.ref</literal> file is examined depending on the current platform.
       </para>

   <itemizedlist>

       <listitem>
       <para>
       on windows platforms: <literal>.win.dia.ref</literal>,
       </para>
       </listitem>

       <listitem>
       <para>
      on Max OS X platforms: <literal>.unix.dia.ref</literal>, <literal>.macosx.dia.ref</literal>,
       </para>
       </listitem>
       
       <listitem>
       <para>
       on Linux platforms: <literal>.unix.dia.ref</literal>, <literal>.linux.dia.ref</literal>.
       </para>
       </listitem>
      
     </itemizedlist>

  </refsection>

<refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
// Launch all tests
// =============================================

test_run();
test_run([]);
test_run([],[]);

// Test one or several module
// =============================================

// Test one module
test_run('time');

// Test several modules
test_run(['time','string']);

// Test a submodule
test_run(['optimization|neldermead']);

// Refer to a module by its path
test_run(SCI+'/modules/core');

// Launch a specific test
// =============================================

// One specific test
test_run('time','datenum');

// Several tests
test_run('time',['datenum';'calendar']);

// Skip some tests
// =============================================

test_run('time',['datenum';'calendar'],'skip_tests');

// Options
// =============================================

// does not check if the .dia and .dia.ref are equal
test_run('time','datenum','no_check_ref');

// Create the .dia.ref file and does not check if the .dia and .dia.ref are equal
test_run([],[],'create_ref');

// Does not perform the tests but displays a list of available tests
test_run([],[],'list');

// Display some examples about how to use this command
test_run([],[],'help');

// Runs only the non-regression tests, skipping unit tests
test_run([],[],'nonreg_test');

// Runs only the unit tests, skipping non-regression tests
test_run([],[],'unit_test');

// Do not check the error output (std err)
test_run('boolean','bug_2799','no_check_error_output');

// Combine several options
test_run([],[],['no_check_ref','mode_nw']);
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>Internal Design</title>
    <para>
    The tests are performed in the temporary directory, not in the directory 
    which originaly contain the tests files.
    The .tst file is copied into the temporary directory, the test is performed
    and the .dia.ref is copied back into the original location.
    </para>

    <para>
    The .tst script is not run as is. Instead, a header and a footer are 
    inserted at the beginning and at the end of the .tst at the time 
    the script is copied into the temporary directory.
    The role of this modification is to redirect the output messages
    into the .dia file, so that the user can have a log file once the test
    is performed.
    </para>

  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Pierre Maréchal</member>
      <member>Michael Baudin</member>
    </simplelist>
  </refsection>
</refentry>
