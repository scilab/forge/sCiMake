// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Generate a cleaner.sce script for the toolbox
function tbx_build_cleaner(toolbox_name, toolbox_path)
	oldpath = pwd();
	
	if(exists('toolbox_path', 'local')) then
		chdir(toolbox_path);
	end
	
	mprintf(gettext('Generating cleaner.sce...\n'));
	cleaner = [
	  "// ====================================================================",
	  "// generated by builder.sce",
	  "// Copyright DIGITEO 2009",
	  "// ====================================================================",
	  "try",
	  " getversion(''scilab'');",
	  "catch",
	  " warning(''Scilab 5.0 or more is required.'');",
	  " return;",
	  "end;",
	  "// ====================================================================",
	  "root_tlbx = get_absolute_file_path(''cleaner.sce'');",
	  "",
	  "if fileinfo(root_tlbx+''/macros/cleanmacros.sce'') <> [] then",
	  "  exec(root_tlbx+''/macros/cleanmacros.sce'');",
	  "end",
	  "",
	  "if fileinfo(root_tlbx+''/src/cleaner_src.sce'') <> [] then",
	  "  exec(root_tlbx+''/src/cleaner_src.sce'');",
	  "end",
	  "",
	  "if fileinfo(root_tlbx+''/sci_gateway/cleaner_gateway.sce'') <> [] then",
	  "  exec(root_tlbx+''/sci_gateway/cleaner_gateway.sce'');",
	  "  mdelete(root_tlbx+''/sci_gateway/cleaner_gateway.sce'');",
	  "end",
	  "",
	  "if fileinfo(root_tlbx+''/loader.sce'') <> [] then",
	  "  mdelete(root_tlbx+''/loader.sce'');",
	  "end",
	  "// ====================================================================",
	  "clear root_tlbx;",
	  "// ===================================================================="
	];
	mputl(cleaner,'cleaner.sce');
	
	chdir(oldpath);
endfunction
