// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Generate a cleaner_gateway.sce script for the gateway
function tbx_build_gateway_clean(langs, gateway_path)
	oldpath = pwd();
	
	if(exists('gateway_path', 'local')) then
		chdir(gateway_path);
	end
	
	mprintf(gettext('Generating cleaner_gateway.sce...\n'));
	cleaner = [
	  "// This file is released into the public domain",
	  "// Generated by cleaner_gateway.sce: Please, do not edit this file",
	  "//",
	  "sci_gateway_dir = get_absolute_file_path(''cleaner_gateway.sce'');",
	  "current_dir     = pwd();",
	  ""];
	
	for i = 1:size(langs, "*")
		cleaner = [cleaner, 
		  "chdir(sci_gateway_dir);",
		  "if ( isdir(''"+langs(i)+"'') ) then",
		  "  chdir(''"+langs(i)+"'');",
		  "  exec(''cleaner.sce'');",
		  "  mdelete(''cleaner.sce'');",
		  "end",
		  ""
		];
	end
	
	cleaner = [cleaner,
	  "chdir(current_dir);",
	  "// ====================================================================",
	  "clear sci_gateway_dir;",
	  "clear current_dir;",
	  "// ===================================================================="
	];
	
	mputl(cleaner,'cleaner_gateway.sce');
	
	chdir(oldpath);
endfunction
