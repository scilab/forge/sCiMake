// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA - Simon LIPP <simon.lipp@scilab.org>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Generate a loader_gateway.sce script for the gateway
function tbx_build_gateway_loader(langs, gateway_path)
	oldpath = pwd();
	
	if(exists('gateway_path', 'local')) then
		chdir(gateway_path);
	end
	
	mprintf(gettext('Generating loader_gateway.sce...\n'));
	loader = [
	  "// This file is released into the public domain",
	  "// Generated by builder_gateway.sce: Please, do not edit this file",
	  "//",
	  "",
	  "try"
	  "  v = getversion(''scilab'');",
	  "catch",
	  "  v = [ 5 0 ]; // or older ",
	  "end",
 	  "if (v(1) <= 5) & (v(2) < 2) then",
	  "  // new API in scilab 5.2",
	  "  error(gettext(''Scilab 5.2 or more is required.''));",
	  "end",
	  "",
	  "sci_gateway_dir = get_absolute_file_path(''loader_gateway.sce'');",
	  "current_dir     = pwd();",
	  ""];
	
	for i = 1:size(langs, "*")
		loader = [loader, 
		  "chdir(sci_gateway_dir);",
		  "if ( isdir(''"+langs(i)+"'') ) then",
		  "  chdir(''"+langs(i)+"'');",
		  "  exec(''loader.sce'');",
		  "end",
		  ""
		];
	end
	
	loader = [loader,
	  "chdir(current_dir);",
	  "// ====================================================================",
	  "clear sci_gateway_dir;",
	  "clear current_dir;",
	  "// ===================================================================="
	];
	
	mputl(loader,'loader_gateway.sce');
	
	chdir(oldpath);
endfunction
