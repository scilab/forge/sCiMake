<?xml version="1.0" encoding="ISO-8859-1"?>
<book version="5.0-subset Scilab" xml:lang="en"
      xmlns="http://docbook.org/ns/docbook"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:xi="http://www.w3.org/2001/XInclude"
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      xmlns:html="http://www.w3.org/1999/xhtml"
      xmlns:db="http://docbook.org/ns/docbook">
  <title>doc_kit, a toolkit for Scilab documentation writers</title>

  <chapter xml:id="intro">
    <title>Introduction</title>

    <para>This toolkit contains all the <link linkend="tools">command-line
    tools</link> needed to write some documentation for <link
    xlink:href="http://www.scilab.org/">Scilab</link>.</para>

    <para>As of version 5, Scilab uses a <link linkend="db5subset">strict
    subset of DocBook 5</link> for all its documentation. This toolkit also
    contains tools (<link
    linkend="manrev2sci"><literal>manrev2sci</literal></link> and <link
    linkend="man2sci"><literal>man2sci</literal></link>) which may be used to
    convert from Scilab's old formats (<literal>manrev.dtd</literal> and
    <literal>man.dtd</literal>) to the new one.</para>
  </chapter>

  <chapter xml:id="install">
    <title>Installation</title>

    <para>This toolkit already includes <emphasis>all</emphasis> the needed
    software components (<link
    xlink:href="http://saxon.sourceforge.net/">Saxon</link>, <link
    xlink:href="http://xmlgraphics.apache.org/fop/">FOP</link>, <link
    xlink:href="http://xmlgraphics.apache.org/batik/">Batik</link>, <link
    xlink:href="http://jeuclid.sourceforge.net/">JEuclid</link>, <link
    xlink:href="http://java.sun.com/products/javahelp/">JavaHelp</link>, <link
    xlink:href="http://www.thaiopensource.com/relaxng/jing.html">Jing</link>,
    etc), therefore you just need to install a
    Java<superscript>TM</superscript> Development Kit (JDK) 1.5+ in order to
    be able to run the command-line tools.</para>

    <para>Note that installing a Java runtime (JRE) is insufficient because
    some command-line tools expect to have the <literal>jar</literal> utility
    (part of the JDK, not part of the JRE) in the
    <literal>PATH</literal>.</para>

    <orderedlist>
      <listitem>
        <para>Unzip the distribution somewhere. This creates a
        <literal>doc_kit/</literal> directory.</para>

        <programlisting>C:\&gt; unzip doc_kit.zip
C:\&gt; dir
...
13/02/2008  14:19    &lt;DIR&gt;          doc_kit
...</programlisting>
      </listitem>

      <listitem>
        <para>All the command-line tools are found in the
        <literal>bin/</literal> subdirectory.</para>

        <programlisting>C:\doc_kit&gt; cd bin
C:\doc_kit\bin&gt; dir /w
...
[.]               [..]              man2sci           sci2jh
sciviewhelp       sci2html          manrev2sci        scivalid
scivalid.bat      manrev2sci.bat    man2sci.bat       sci2html.bat
sciviewhelp.bat   sci2pdf.bat       sci2pdf           sci2jh.bat
sci2chm.bat
...</programlisting>

        <para>All the above command-line tools are intended to be used
        directly from the <literal>doc_kit/bin/</literal> subdirectory.</para>
      </listitem>

      <listitem>
        <para>Test your installation by converting this document
        (<literal>doc_kit/doc/doc.xml</literal>) to a JavaHelp
        <literal>.jar</literal> file.</para>

        <orderedlist numeration="loweralpha">
          <listitem>
            <para>First make sure that the Java tools are actually in your
            <literal>PATH</literal>.</para>

            <programlisting>C:\doc_kit\bin&gt; java -version
java version "1.6.0_04"
Java(TM) SE Runtime Environment (build 1.6.0_04-b12)
Java HotSpot(TM) Client VM (build 10.0-b19, mixed mode, sharing)

C:\doc_kit\bin&gt; jar
Usage: jar {ctxui}[vfm0Me] ...</programlisting>
          </listitem>

          <listitem>
            <para>Convert this document
            (<literal>doc_kit/doc/doc.xml</literal>) to JavaHelp using the
            <link linkend="sci2jh"><literal>sci2jh</literal></link>
            command-line tool.</para>

            <programlisting>C:\doc_kit\bin&gt; sci2jh ..\doc\doc.xml C:\tmp\test_help.jar</programlisting>

            <para>Note that the generated JavaHelp file must end with
            "<literal>_help.jar</literal>" and not just with
            "<literal>.jar</literal>".</para>
          </listitem>

          <listitem>
            <para>Preview the contents of the generated
            <literal>.jar</literal> file using the <link
            linkend="sciviewhelp"><literal>sciviewhelp</literal></link>
            tool.</para>

            <programlisting>C:\doc_kit\bin&gt; sciviewhelp C:\tmp\test_help.jar</programlisting>
          </listitem>
        </orderedlist>
      </listitem>
    </orderedlist>
  </chapter>

  <chapter xml:id="distrib">
    <title>Contents of the distribution</title>

    <variablelist>
      <varlistentry>
        <term>bin/</term>

        <listitem>
          <para>Contains the command-line tools: shell scripts for use on
          Unix/Linux/Mac OS X and <literal>.bat</literal> files for use on
          Windows (2000, XP, Vista).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>css/</term>

        <listitem>
          <para>Contains the CSS style sheets which are used to customize the
          look of the HTML pages generated by <link
          linkend="sci2html"><literal>sci2html</literal></link>, <link
          linkend="sci2chm"><literal>sci2chm</literal></link> and <link
          linkend="sci2jh"><literal>sci2jh</literal></link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>doc/</term>

        <listitem>
          <para>Contains this document in various formats.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>docbook_xsl/</term>

        <listitem>
          <para>Contains a copy of the <link
          xlink:href="http://docbook.sourceforge.net/">DocBook XSL style
          sheets</link>. These style sheets are used to convert DocBook 5
          documents (and hence, Scilab documentation) to HTML, PostScript,
          PDF, JavaHelp, etc.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>legal/</term>

        <listitem>
          <para>Contains a README and a LICENSE file for each software
          component bundled with this toolkit.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>lib/</term>

        <listitem>
          <para>Contains all the software components (that is, Java class
          libraries packaged as <literal>.jar</literal> files) bundled with
          this toolkit.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>schema/</term>

        <listitem>
          <para>Contains <literal>scilab.rnc</literal>, the RELAX NG schema
          defining the DocBook 5 subset used for the documentation of
          Scilab.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>src/</term>

        <listitem>
          <para>Contains the source code of two utility classes needed by the
          command-line tools of this toolkit. More information in "<link
          linkend="build">Recompiling the sources</link>".</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>xsl/</term>

        <listitem>
          <variablelist>
            <varlistentry>
              <term>convert/manrev2sci.xsl</term>

              <listitem>
                <para>The XSLT style sheet used to convert a document
                conforming to <literal>manrev.dtd</literal> to a document
                conforming to the DocBook 5 subset.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>convert/man2manrevxsl</term>

              <listitem>
                <para>The XSLT style sheet used to convert a document
                conforming to <literal>man.dtd</literal> to a document
                conforming to <literal>manrev.dtd</literal>.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>fo/docbook.xsl</term>

              <listitem>
                <para>Fixes bugs in the stock
                <literal>docbook/fo/htmltbl.xsl</literal>.</para>
              </listitem>
            </varlistentry>
          </variablelist>
        </listitem>
      </varlistentry>
    </variablelist>
  </chapter>

  <chapter xml:id="db5subset">
    <title>The Scilab subset of DocBook 5</title>

    <para>The documentation of Scilab must be written using the strict subset
    of DocBook 5 defined in <literal>doc_kit/schema/scilab.rnc</literal>.
    DocBook 5 elements are fully documented in "<link
    xlink:href="http://docbook.org/tdg5/en/html/docbook.html">DocBook 5.0: The
    Definitive Guide</link>", therefore there is not much to add here.</para>

    <note>
      <title>Important</title>

      <para>The root element of a document which conforms to the Scilab
      DocBook 5 subset must have <literal>version</literal> attribute set to
      "<literal>5.0-subset Scilab</literal>". Example:</para>

      <programlisting>&lt;?xml version="1.0" encoding="UTF-8" ?&gt;
&lt;refentry version="5.0-subset Scilab" 
          xmlns="http://docbook.org/ns/docbook"
          ...</programlisting>
    </note>

    <para>The subset comprises the following elements:</para>

    <itemizedlist>
      <listitem>
        <para><literal>book</literal> and its divisions:</para>

        <itemizedlist>
          <listitem>
            <para><literal>part</literal>, <literal>partintro</literal></para>
          </listitem>

          <listitem>
            <para><literal>reference</literal></para>
          </listitem>

          <listitem>
            <para><literal>chapter</literal>,
            <literal>section</literal></para>
          </listitem>

          <listitem>
            <para><literal>appendix</literal></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Meta-info elements: <literal>info</literal>,
        <literal>title</literal>, <literal>author</literal>,
        <literal>personname</literal>, <literal>affiliation</literal>,
        <literal>jobtitle</literal>, <literal>orgname</literal>,
        <literal>pubdate</literal>, <literal>keywordset</literal>,
        <literal>keyword</literal>.</para>
      </listitem>

      <listitem>
        <para><literal>refentry</literal> (the DocBook equivalent of a man
        page) and its divisions: <literal>refnamediv</literal>,
        <literal>refname</literal>, <literal>refpurpose</literal>,
        <literal>refsynopsisdiv</literal>, <literal>synopsis</literal>,
        <literal>refsection</literal>.</para>
      </listitem>

      <listitem>
        <para>Displayed elements:</para>

        <itemizedlist>
          <listitem>
            <para><literal>figure</literal>, <literal>mediaobject</literal>,
            <literal>imageobject</literal>, <literal>imagedata</literal>
            (having either a <literal>fileref</literal> attribute or embedding
            <link xlink:href="http://www.w3.org/Math/">MathML</link> or <link
            xlink:href="http://www.w3.org/Graphics/SVG/">SVG</link>)</para>
          </listitem>

          <listitem>
            <para><literal>example</literal></para>
          </listitem>

          <listitem>
            <para><literal>note</literal></para>
          </listitem>

          <listitem>
            <para><literal>equation</literal>,
            <literal>informalequation</literal></para>
          </listitem>

          <listitem>
            <para><literal>table</literal> (HTML tables only, that is, not
            CALS tables), <literal>caption</literal>,
            <literal>informaltable</literal>, <literal>col</literal>,
            <literal>colgroup</literal>, <literal>tbody</literal>,
            <literal>thead</literal>, <literal>tfoot</literal>,
            <literal>tr</literal>, <literal>td</literal>,
            <literal>th</literal></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Other block-level elements:</para>

        <itemizedlist>
          <listitem>
            <para><literal>itemizedlist</literal>,
            <literal>orderedlist</literal>, <literal>listitem</literal></para>
          </listitem>

          <listitem>
            <para><literal>variablelist</literal>,
            <literal>varlistentry</literal>, <literal>term</literal></para>
          </listitem>

          <listitem>
            <para><literal>simplelist</literal>,
            <literal>member</literal></para>
          </listitem>

          <listitem>
            <para><literal>para</literal></para>
          </listitem>

          <listitem>
            <para><literal>programlisting</literal></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Inline-level elements:</para>

        <itemizedlist>
          <listitem>
            <para><literal>emphasis</literal>,
            <literal>literal</literal></para>
          </listitem>

          <listitem>
            <para><literal>phrase</literal>,
            <literal>replaceable</literal>,</para>
          </listitem>

          <listitem>
            <para><literal>subscript</literal>,
            <literal>superscript</literal></para>
          </listitem>

          <listitem>
            <para><literal>link</literal></para>
          </listitem>

          <listitem>
            <para><literal>indexterm</literal>,
            <literal>primary</literal></para>
          </listitem>

          <listitem>
            <para><literal>inlinemediaobject</literal></para>
          </listitem>

          <listitem>
            <para><literal>inlineequation</literal></para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </chapter>

  <chapter xml:id="tools">
    <title>The command-line tools</title>

    <para>All command-line tools are self-documented. Simply execute the
    command without any argument to display a short help text. Example:</para>

    <programlisting>C:\doc_kit\bin&gt; sciviewhelp
Usage: sciviewhelp javahelp_jar_file ... javahelp_jar_file
Allows to browse the contents of one or more
JavaHelp[tm] .jar files created using sci2jh.
The name of a JavaHelp .jar file must end with '_help.jar'.</programlisting>

    <variablelist>
      <varlistentry xml:id="man2sci">
        <term>man2sci</term>

        <term>man2sci.bat</term>

        <listitem>
          <para>Converts a document conforming to <literal>man.dtd</literal>
          to a document conforming to <literal>scilab.rnc</literal>.</para>

          <para>Usage: <literal>man2sci
          <replaceable>in_man_xml_file</replaceable>
          <replaceable>out_scilab_xml_file</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="manrev2sci">
        <term>manrev2sci</term>

        <term>manrev2sci.bat</term>

        <listitem>
          <para>Converts a document conforming to
          <literal>manrev.dtd</literal> to a document conforming to
          <literal>scilab.rnc</literal>.</para>

          <para>Usage: <literal>manrev2sci
          <replaceable>in_manrev_xml_file</replaceable>
          <replaceable>out_scilab_xml_file</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="sci2html">
        <term>sci2html</term>

        <term>sci2html.bat</term>

        <listitem>
          <para>Converts an XML document conforming to
          <literal>scilab.rnc</literal> to multi-page HTML.</para>

          <para>Usage: <literal>sci2html
          <replaceable>in_xml_file</replaceable>
          <replaceable>out_html_directory</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="sci2chm">
        <term>sci2chm.bat</term>

        <listitem>
          <para>Converts an XML document conforming to
          <literal>scilab.rnc</literal> to a Windows HTML Help
          (".<literal>chm</literal>") file. Not available on platforms other
          than Windows.</para>

          <para>By default, this script assumes that
          <literal>hhc.exe</literal> is found in "<literal>C:\Program
          Files\HTML Help Workshop\hhc.exe</literal>". If this is not the
          case, please modify the "<literal>hhc</literal>" variable found at
          the beginning of the "<literal>.bat</literal>" file.</para>

          <para>Usage: <literal>sci2chm <replaceable>in_xml_file</replaceable>
          <replaceable>out_chm_file</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="sci2jh">
        <term>sci2jh</term>

        <term>sci2jh.bat</term>

        <listitem>
          <para>Converts an XML document conforming to
          <literal>scilab.rnc</literal> to a JavaHelp <literal>.jar</literal>
          file. <literal>out_javahelp_jar_file</literal> must end with
          "<literal>_help.jar</literal>".</para>

          <para>Usage: <literal>sci2jh <replaceable>in_xml_file</replaceable>
          <replaceable>out_javahelp_jar_file</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="sci2pdf">
        <term>sci2pdf</term>

        <term>sci2pdf.bat</term>

        <listitem>
          <para>Converts an XML document conforming to
          <literal>scilab.rnc</literal> to PDF or to PostScript. A PostScript
          file is generated if <replaceable>out_pdf_or_ps_file</replaceable>
          ends with "<literal>.ps</literal>".</para>

          <para>Usage: <literal>sci2pdf <replaceable>in_xml_file</replaceable>
          <replaceable>out_pdf_or_ps_file</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="scivalid">
        <term>scivalid</term>

        <term>scivalid.bat</term>

        <listitem>
          <para>Validates specified XML files against the
          <literal>scilab.rnc</literal> schema.</para>

          <para>Usage: <literal>scivalid <replaceable>xml_file</replaceable>
          ... <replaceable>xml_file</replaceable></literal></para>
        </listitem>
      </varlistentry>

      <varlistentry xml:id="sciviewhelp">
        <term>sciviewhelp</term>

        <term>sciviewhelp.bat</term>

        <listitem>
          <para>Allows to browse the contents of one or more JavaHelp .jar
          files created using <literal>sci2jh</literal>. The name of a
          JavaHelp <literal>.jar</literal> file must end with
          "<literal>_help.jar</literal>".</para>

          <para>Usage: <literal>sciviewhelp
          <replaceable>javahelp_jar_file</replaceable> ...
          <replaceable>javahelp_jar_file</replaceable></literal></para>
        </listitem>
      </varlistentry>
    </variablelist>
  </chapter>

  <chapter xml:id="build">
    <title>Recompiling the sources</title>

    <para>Directory <literal>doc_kit/src/org/scilab/doc_kit/</literal>
    contains the source code of two utility classes needed by some of the
    above command-line tools:</para>

    <variablelist>
      <varlistentry>
        <term>HelpViewer.java</term>

        <listitem>
          <para>The source code of <link
          linkend="sciviewhelp"><literal>sciviewhelp</literal></link>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>CopyConvert.java</term>

        <listitem>
          <para>A preprocessor used as the first pass in <link
          linkend="sci2html"><literal>sci2html</literal></link>, <link
          linkend="sci2chm"><literal>sci2chm</literal></link>, <link
          linkend="sci2jh"><literal>sci2jh</literal></link> and <link
          linkend="sci2pdf"><literal>sci2pdf</literal></link>.</para>

          <para>This preprocessor:</para>

          <itemizedlist>
            <listitem>
              <para>Creates a ``flat' (entity references are expanded; <link
              xlink:href="http://www.w3.org/TR/xinclude/">XIncludes</link> are
              not supported) copy of the XML document to be
              transformed.</para>
            </listitem>

            <listitem>
              <para>If the source document contains an embedded MathML element
              (<literal>imagedata/mml:math</literal>), this element is
              extracted to a temporary file, then the temporary file is
              converted to a PNG image by the means of <link
              xlink:href="http://jeuclid.sourceforge.net/">JEuclid</link>.
              After doing this, the original
              <literal>imagedata/mml:math</literal> found in the source is
              replaced in the flat XML document by an
              <literal>imagedata</literal> element pointing to the generated
              PNG file (<literal>imagedata/@fileref</literal>).</para>
            </listitem>

            <listitem>
              <para>Same for embedded SVG elements which are converted to PNG
              images by the means of <link
              xlink:href="http://xmlgraphics.apache.org/batik/">Batik</link>.</para>
            </listitem>

            <listitem>
              <para>If the source document contains an
              <literal>imagedata</literal> element pointing to a MathML file
              (filename extension is "<literal>.mml</literal>"), this file is
              converted to PNG by the means of <link
              xlink:href="http://jeuclid.sourceforge.net/">JEuclid</link>.
              After doing that, the <literal>imagedata</literal> element
              contained in the flat XML document is made to point to the
              <literal>.png</literal> file rather than to the
              <literal>.mml</literal> file.</para>
            </listitem>

            <listitem>
              <para>Same for <literal>imagedata</literal> elements pointing to
              SVG files (filename extension is "<literal>.svg</literal>" or
              "<literal>.svgz</literal>"), but in this case, it is <link
              xlink:href="http://xmlgraphics.apache.org/batik/">Batik</link>
              which is used to perform the conversion.</para>
            </listitem>

            <listitem>
              <para>Same for <literal>imagedata</literal> elements pointing to
              "<literal>.tex</literal>" files (assumed to contain math
              equations), but in this case, it is <link
              xlink:href="http://www.tug.org/">T<subscript>E</subscript>X</link>+<link
              xlink:href="http://pages.cs.wisc.edu/~ghost/">Ghostscript</link>
              which are used to perform the conversion.</para>

              <para>The <literal>CopyConvert</literal> preprocessor searches
              the following programs in the <literal>PATH</literal>:
              <literal>latex</literal>, <literal>dvips</literal>,
              <literal>gs</literal> on Unix/<literal>gswin32c</literal> on
              Windows, <literal>ps2pdf</literal>.</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>The above sources may be recompiled by running <link
    xlink:href="http://ant.apache.org/">ant</link> in the
    <literal>doc_kit/src/</literal> directory:</para>

    <programlisting>C:\doc_kit/src&gt; dir /w
...
build.xml   [org]       [class]
...
C:\doc_kit/src&gt; ant
Buildfile: build.xml

init:

compile:
...</programlisting>

    <para>Doing this rebuilds <literal>sci_doc_kit.jar</literal> in
    <literal>doc_kit/lib/</literal>.</para>
  </chapter>

  <chapter xml:id="other_tools">
    <title>Companion tools</title>

    <itemizedlist>
      <listitem>
        <para><link xlink:href="http://www.inkscape.org/">Inkscape</link>, an
        excellent drawing tool natively supporting SVG.</para>
      </listitem>

      <listitem>
        <para><link xlink:href="http://miktex.org/">MiKTeX</link>, an
        excellent T<subscript>E</subscript>X distribution for Windows.</para>
      </listitem>

      <listitem>
        <para><link xlink:href="http://www.xmlmind.com/xmleditor/">XMLmind XML
        Editor</link>, a visual XML editor with built-in support for DocBook
        5. There are many fine XML editors but this one</para>

        <itemizedlist>
          <listitem>
            <para>has a free <link
            xlink:href="http://www.xmlmind.com/xmleditor/persoedition.html">Personal
            Edition</link> allowing to use it to create documentation for Open
            Source projects such as Scilab,</para>
          </listitem>

          <listitem>
            <para>has a configuration specially designed to support the Scilab
            DocBook 5 subset.</para>
          </listitem>
        </itemizedlist>

        <para>This Scilab configuration is available as an add-on. This add-on
        may be downloaded and installed directly within XMLmind XML Editor
        using menu item <emphasis role="bold">Options</emphasis>|<emphasis
        role="bold">Install Add-ons</emphasis>. But before being able to do
        that, you must specify where to find such add-on. Fortunately this is
        done once for all:</para>

        <orderedlist>
          <listitem>
            <para>Use menu item <emphasis
            role="bold">Options</emphasis>|<emphasis
            role="bold">Preferences</emphasis>.</para>
          </listitem>

          <listitem>
            <para>Click on the "<emphasis role="bold">Install
            Add-ons</emphasis>" item in the left pane.</para>

            <mediaobject>
              <imageobject>
                <imagedata fileref="addonprefs.png" />
              </imageobject>
            </mediaobject>
          </listitem>

          <listitem>
            <para>Click the <emphasis role="bold">Add</emphasis> button found
            at the right of the "<emphasis role="bold">Download add-ons from
            these servers</emphasis>" list.</para>
          </listitem>

          <listitem>
            <para>Specify the following URL
            "<literal>http://www.scilab.org/download/scilab_config.xxe_addon</literal>"
            when prompted, then click <emphasis
            role="bold">OK</emphasis>.</para>
          </listitem>

          <listitem>
            <para>Click <emphasis role="bold">OK</emphasis> to close the
            <emphasis role="bold">Preferences</emphasis> dialog box.</para>
          </listitem>
        </orderedlist>

        <para>From now, you can use menu item <emphasis
        role="bold">Options</emphasis>|<emphasis role="bold">Install
        Add-ons</emphasis> and select the add-on called "<emphasis
        role="bold">A configuration for the Scilab subset of DocBook
        5</emphasis>" from the list of all available add-ons.</para>
      </listitem>
    </itemizedlist>
  </chapter>
</book>