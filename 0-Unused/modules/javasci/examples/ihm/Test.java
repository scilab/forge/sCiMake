/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA 
 * 
 * This file is released into the public domain
 *
 */


public class Test
{
  public static void main(String[] args)
  {
    MaFenetre F = new MaFenetre();
    F.setVisible(true);
  }
}
