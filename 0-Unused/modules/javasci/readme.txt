This module provides capabilities to call Scilab from Java.

Please have a look to the help of this module.

Note that this module is based on the module call_scilab.

Some examples are available in SCI/modules/javasci/examples/ and can be built 
in the Scilab source tree with the command "ant build-examples" from
the directory modules/javasci/
