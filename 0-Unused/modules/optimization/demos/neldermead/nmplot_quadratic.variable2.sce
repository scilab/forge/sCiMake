// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


mprintf("Illustrates that the Nelder-Mead algorithm performs correctly on a badly quadratic test case.\n");
mprintf("Defining quadratic function...\n");
a = 100.0;
function [ y , index ] = quadratic ( x , index )
  y = a * x(1)^2 + x(2)^2;
endfunction

mprintf("Creating nmplot object...\n");
nm = nmplot_new ();
nm = nmplot_configure(nm,"-numberofvariables",2);
nm = nmplot_configure(nm,"-function",quadratic);
nm = nmplot_configure(nm,"-x0",[10.0 10.0]');
nm = nmplot_configure(nm,"-maxiter",400);
nm = nmplot_configure(nm,"-maxfunevals",400);
nm = nmplot_configure(nm,"-tolxmethod",%f);
nm = nmplot_configure(nm,"-tolsimplexizerelative",1.e-8);
nm = nmplot_configure(nm,"-simplex0method","spendley");
//
// Perform optimization
//
mprintf("Searching (please wait)...\n");
nm = nmplot_search(nm);
nmplot_display(nm);
nm = nmplot_destroy(nm);
mprintf("End of demo.\n");

//
// Load this script into the editor
//
filename = 'nmplot_quadratic.variable2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

