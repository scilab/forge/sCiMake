//
// rosenbrock.history.fbar.txt--
//   History of the function value average during Nelder-Mead algorithm.
//
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
history = [
0 3.784675e+000
1 3.784675e+000
2 1.994637e+000
2 1.994637e+000
]
