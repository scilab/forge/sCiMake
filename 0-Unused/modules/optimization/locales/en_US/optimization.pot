# Localization of the module optimization
# Please see in SCI/tools/localization for localization management
# Copyright (C) 2007/2008 - INRIA / Scilab
# This file is distributed under the same license as the Scilab package.
# Sylvestre Ledru <sylvestre.ledru@inria.fr>, 2007/2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Scilab\n"
"Report-Msgid-Bugs-To: <localization@scilab.org>\n"
"POT-Creation-Date: 2007-11-26 18:05+0100\n"
"Last-Translator: Sylvestre Ledru <sylvestre.ledru@inria.fr>\n"
"Language-Team: Scilab Localization <localization@scilab.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Revision-Date: 2009-12-09 16:34+0100\n"

# File: src/c/sp.c, line: 468
msgid ""
"\n"
"    primal obj.  dual obj.  dual. gap \n"
msgstr ""
#
# File: macros/lmisolver.sci, line: 169
#, c-format
msgid " number of of constraints (%s)'),'lmisolver',%m,%fm));"
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 913
#, c-format
msgid "%s: %s"
msgstr ""
#
# File: sci_gateway/c/sci_qld.c, line: 150
#, c-format
msgid "%s: Accuracy insufficient to satisfy convergence criterion.\n"
msgstr ""
#
# File: macros/neldermead/optimget.sci, line: 47
#, c-format
msgid "%s: Ambiguous property name %s matches several fields : %s"
msgstr ""
#
# File: sci_gateway/c/sci_qp_solve.c, line: 81
#, c-format
msgid "%s: Argument 3: wrong number of columns %d expected\n"
msgstr ""
#
# File: sci_gateway/c/sci_qp_solve.c, line: 110
# File: sci_gateway/c/sci_qp_solve.c, line: 134
# File: sci_gateway/c/sci_qp_solve.c, line: 142
#, c-format
msgid "%s: Cannot allocate more memory.\n"
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 18
#, c-format
msgid "%s: Cannot check cost function when x0 is empty"
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 30
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 71
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 93
#, c-format
msgid "%s: Cannot evaluate cost function from costf(x0,1)."
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 38
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 79
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 101
#, c-format
msgid "%s: Cannot evaluate cost function from costf(x0,2)."
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 110
#, c-format
msgid "%s: Cannot evaluate cost function from costf(x0,3)."
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 119
#, c-format
msgid "%s: Cannot evaluate cost function from costf(x0,4)."
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 47
#, c-format
msgid "%s: Cannot evaluate cost function from costf(x0,5)."
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 56
#, c-format
msgid "%s: Cannot evaluate cost function from costf(x0,6)."
msgstr ""
#
# File: macros/optimbase/optimbase_function.sci, line: 92
#, c-format
msgid "%s: Empty function (use -function option)."
msgstr ""
#
# File: macros/neldermead/neldermead_configure.sci, line: 224
# File: macros/optimbase/optimbase_configure.sci, line: 139
# File: macros/optimsimplex/optimsimplex_new.sci, line: 590
#, c-format
msgid "%s: Expected boolean but got %s instead"
msgstr ""
#
# File: macros/neldermead/neldermead_configure.sci, line: 217
# File: macros/optimbase/optimbase_configure.sci, line: 132
# File: macros/optimsimplex/optimsimplex_new.sci, line: 583
#, c-format
msgid "%s: Expected function but got %s instead"
msgstr ""
#
# File: macros/neldermead/neldermead_configure.sci, line: 203
# File: macros/optimbase/optimbase_configure.sci, line: 118
# File: macros/optimsimplex/optimsimplex_new.sci, line: 569
#, c-format
msgid "%s: Expected real variable but got %s instead"
msgstr ""
#
# File: macros/neldermead/neldermead_configure.sci, line: 210
# File: macros/optimbase/optimbase_configure.sci, line: 125
# File: macros/optimsimplex/optimsimplex_new.sci, line: 576
#, c-format
msgid "%s: Expected string variable but got %s instead"
msgstr ""
#
# File: macros/optimbase/optimbase_get.sci, line: 30
# File: macros/optimbase/optimbase_get.sci, line: 37
#, c-format
msgid "%s: History disabled ; enable -storehistory option."
msgstr ""
#
# File: macros/neldermead/neldermead_get.sci, line: 21
# File: macros/optimbase/optimbase_histget.sci, line: 21
# File: macros/optimbase/optimbase_histset.sci, line: 22
# File: macros/optimbase/optimbase_set.sci, line: 26
# File: macros/optimbase/optimbase_set.sci, line: 33
#, c-format
msgid "%s: History disabled ; turn on -storehistory option."
msgstr ""
#
# File: macros/neldermead/neldermead_updatesimp.sci, line: 80
# File: macros/neldermead/neldermead_search.sci, line: 1005
# File: macros/neldermead/neldermead_search.sci, line: 1049
#, c-format
msgid ""
"%s: Impossible to scale the vertex #%d/%d at [%s] into inequality constraints"
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 956
#, c-format
msgid "%s: Initial guess [%s] is not feasible."
msgstr ""
#
# File: macros/fit_dat.sci, line: 100
#, c-format
msgid "%s: Initial guess not feasible.\n"
msgstr ""
#
# File: sci_gateway/c/sci_qld.c, line: 152
#, c-format
msgid "%s: Length of working array is too short.\n"
msgstr ""
#
# File: macros/optimbase/optimbase_logstartup.sci, line: 24
#, c-format
msgid "%s: Log file handle non zero while starting up the logging."
msgstr ""
#
# File: macros/optimbase/optimbase_logstartup.sci, line: 19
#, c-format
msgid "%s: Logging already started."
msgstr ""
#
# File: macros/optimbase/optimbase_logshutdown.sci, line: 18
#, c-format
msgid "%s: Logging not started."
msgstr ""
#
# File: macros/optimbase/optimbase_histget.sci, line: 25
#, c-format
msgid "%s: Negative iteration index %d is not allowed."
msgstr ""
#
# File: macros/optimbase/optimbase_histset.sci, line: 26
#, c-format
msgid "%s: Negative iteration index are not allowed."
msgstr ""
#
# File: macros/neldermead/optimset.sci, line: 156
#, c-format
msgid ""
"%s: No default options available: the function ''%s'' does not exist on the "
"path."
msgstr ""
#
# File: macros/optimbase/optimbase_outstruct.sci, line: 19
#, c-format
msgid "%s: No output command is defined."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_check.sci, line: 33
#, c-format
msgid "%s: Number of columns of fv is %d, which is different from 1."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_check.sci, line: 25
#, c-format
msgid ""
"%s: Number of columns of x is %d, which is different from dimension = %d."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_check.sci, line: 30
#, c-format
msgid ""
"%s: Number of rows of fv is %d, which is different from number of vertices = "
"%d."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_check.sci, line: 22
#, c-format
msgid ""
"%s: Number of rows of x is %d, which is different from number of vertices = %"
"d."
msgstr ""
#
# File: macros/neldermead/optimset.sci, line: 47
#, c-format
msgid ""
"%s: Odd number of arguments : the first argument is expected to be a struct, "
"but is a %s"
msgstr ""
#
# File: macros/derivative.sci, line: 50
#, c-format
msgid "%s: Order must be 1, 2 or 4.\n"
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 418
#, c-format
msgid "%s: Problem has constraints, but fixed algorithm ignores them."
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 173
#, c-format
msgid "%s: Problem has constraints, but variable algorithm ignores them."
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 1177
#, c-format
msgid "%s: Problem has no constraints, but Box algorithm is designed for them."
msgstr ""
#
# File: sci_gateway/c/sci_qp_solve.c, line: 174
#, c-format
msgid "%s: Q is not symmetric positive definite.\n"
msgstr ""
#
# File: macros/neldermead/neldermead_updatesimp.sci, line: 42
# File: macros/neldermead/neldermead_search.sci, line: 939
#, c-format
msgid "%s: Randomized bounds initial simplex is not available without bounds."
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 42
#, c-format
msgid ""
"%s: The -withderivatives option is true but all algorithms in neldermead are "
"derivative-free."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 478
#, c-format
msgid ""
"%s: The boundsmax vector is expected to be a row matrix, but current shape "
"is %d x %d"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 474
#, c-format
msgid ""
"%s: The boundsmin vector is expected to be a row matrix, but current shape "
"is %d x %d"
msgstr ""
#
# File: sci_gateway/c/sci_qld.c, line: 154
#, c-format
msgid "%s: The constraints are inconsistent.\n"
msgstr ""
#
# File: macros/bvodeS.sci, line: 34
#, c-format
msgid "%s: The elements of the argument #%d must be in [%d %d].\n"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_xbar.sci, line: 26
#, c-format
msgid "%s: The exclusion index vector has %d rows instead of 1."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_gradientfv.sci, line: 20
#, c-format
msgid ""
"%s: The gradient can be applied only with a simplex made of n+1 points, but "
"the dimension is %d and the number of vertices is %d"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 295
#, c-format
msgid ""
"%s: The len vector is expected to be a row matrix, but current shape is %d x "
"%d"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 353
#, c-format
msgid ""
"%s: The len vector is expected to be a scalar, but current shape is %d x %d"
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 139
#, c-format
msgid "%s: The matrix %s from costf(x0,%d) has %d columns, instead of %d."
msgstr ""
#
# File: macros/optimbase/optimbase_checkcostfun.sci, line: 135
#, c-format
msgid "%s: The matrix %s from costf(x0,%d) has %d rows, instead of %d."
msgstr ""
#
# File: sci_gateway/c/sci_qp_solve.c, line: 170
#, c-format
msgid "%s: The minimization problem has no solution.\n"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_setallfv.sci, line: 25
#, c-format
msgid ""
"%s: The number of columns in the function value array is %d, while expected "
"1."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_setallx.sci, line: 25
#, c-format
msgid "%s: The number of columns in x is %d, while expected %d."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_setallfv.sci, line: 21
#, c-format
msgid ""
"%s: The number of rows in the function value array is %d, while expected %d."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_setallx.sci, line: 21
#, c-format
msgid "%s: The number of rows in x is %d, while expected %d."
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 183
#, c-format
msgid "%s: The number of variable is zero."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_setall.sci, line: 25
#, c-format
msgid ""
"%s: The number of vertices (i.e. the number of rows) is %d which is smaller "
"than the number of columns %d (i.e. n+1)."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 249
#, c-format
msgid ""
"%s: The numbers of columns of coords is %d but is expected to be at least %d"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 526
#, c-format
msgid ""
"%s: The oriented simplex can be computed only with a simplex made of n+1 "
"points, but the dimension is %d and the number of vertices is %d"
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 238
#, c-format
msgid ""
"%s: The value of the ''OutputFcn'' option is neither a function nor a list."
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 254
#, c-format
msgid ""
"%s: The value of the ''PlotFcns'' option is neither a function nor a list."
msgstr ""
#
# File: macros/optimbase/optimbase_configure.sci, line: 32
#, c-format
msgid ""
"%s: The x0 vector is expected to be a column matrix, but current shape is %d "
"x %d"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 281
# File: macros/optimsimplex/optimsimplex_new.sci, line: 340
# File: macros/optimsimplex/optimsimplex_new.sci, line: 403
# File: macros/optimsimplex/optimsimplex_new.sci, line: 421
# File: macros/optimsimplex/optimsimplex_new.sci, line: 470
#, c-format
msgid ""
"%s: The x0 vector is expected to be a row matrix, but current shape is %d x %"
"d"
msgstr ""
#
# File: sci_gateway/c/sci_qld.c, line: 148
#, c-format
msgid "%s: Too many iterations (more than %d).\n"
msgstr ""
#
# File: macros/karmarkar.sci, line: 56
#, c-format
msgid "%s: Unbounded problem!"
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 68
#, c-format
msgid "%s: Unexpected maximum number of function evaluations %s."
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 58
#, c-format
msgid "%s: Unexpected maximum number of iterations %s."
msgstr ""
#
# File: macros/neldermead/neldermead_configure.sci, line: 148
#, c-format
msgid "%s: Unexpected negative value %s for %s option"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 54
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 0 to 7 are "
"expected."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 201
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 1 to 3 are "
"expected."
msgstr ""
#
# File: macros/optimbase/optimbase_function.sci, line: 84
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 2 are expected."
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 26
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 2 or 3 are "
"expected."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 178
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 2 to 4 are "
"expected."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 72
# File: macros/optimsimplex/optimsimplex_new.sci, line: 97
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 2 to 5 are "
"expected."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 126
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 2 to 6 are "
"expected."
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_new.sci, line: 156
#, c-format
msgid ""
"%s: Unexpected number of input arguments : %d provided while 6 to 7 are "
"expected."
msgstr ""
#
# File: macros/optimbase/optimbase_function.sci, line: 88
#, c-format
msgid ""
"%s: Unexpected number of output arguments : %d provided while 3 to 5 are "
"expected."
msgstr ""
#
# File: macros/neldermead/nmplot_contour.sci, line: 23
#, c-format
msgid ""
"%s: Unexpected number of variables %d. Cannot draw contour plot for "
"functions which do not have two parameters."
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 72
#, c-format
msgid "%s: Unknown -method %s"
msgstr ""
#
# File: macros/neldermead/nmplot_configure.sci, line: 25
# File: macros/optimbase/optimbase_histget.sci, line: 34
# File: macros/optimbase/optimbase_histset.sci, line: 35
# File: macros/optimbase/optimbase_get.sci, line: 49
# File: macros/optimbase/optimbase_configure.sci, line: 111
# File: macros/optimbase/optimbase_cget.sci, line: 65
# File: macros/optimbase/optimbase_set.sci, line: 43
# File: macros/optimsimplex/optimsimplex_new.sci, line: 193
#, c-format
msgid "%s: Unknown key %s"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_gradientfv.sci, line: 36
#, c-format
msgid "%s: Unknown method %s"
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 815
#, c-format
msgid "%s: Unknown restart detection %s"
msgstr ""
#
# File: macros/neldermead/neldermead_updatesimp.sci, line: 50
#, c-format
msgid "%s: Unknown restart simplex method %s"
msgstr ""
#
# File: macros/optimsimplex/optimsimplex_size.sci, line: 59
#, c-format
msgid "%s: Unknown simplex size method %s"
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 136
#, c-format
msgid "%s: Unknown status %s"
msgstr ""
#
# File: macros/neldermead/fminsearch.sci, line: 204
#, c-format
msgid "%s: Unknown step %s"
msgstr ""
#
# File: macros/neldermead/neldermead_configure.sci, line: 232
# File: macros/optimbase/optimbase_configure.sci, line: 145
#, c-format
msgid "%s: Unknown value %s for %s option"
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 965
#, c-format
msgid "%s: Unknown value %s for -scalingsimplex0 option"
msgstr ""
#
# File: macros/neldermead/neldermead_search.sci, line: 945
#, c-format
msgid "%s: Unknown value %s for -simplex0method option"
msgstr ""
#
# File: macros/neldermead/optimset.sci, line: 119
#, c-format
msgid "%s: Unrecognized parameter name ''%s''."
msgstr ""
#
# File: macros/neldermead/optimset.sci, line: 101
#, c-format
msgid "%s: Unrecognized value ''%s'' for ''Display'' option."
msgstr ""
#
# File: macros/fit_dat.sci, line: 81
#, c-format
msgid "%s: Weighting matrix must be square.\n"
msgstr ""
#
# File: macros/neldermead/optimget.sci, line: 24
#, c-format
msgid "%s: Wrong number of arguments : %d expected while %d given"
msgstr ""
#
# File: macros/bvodeS.sci, line: 30
# File: macros/fit_dat.sci, line: 78
#, c-format
msgid "%s: Wrong number of input argument(s).\n"
msgstr ""
#
# File: macros/derivative.sci, line: 38
#, c-format
msgid "%s: Wrong number of input arguments: %d to %d expected.\n"
msgstr ""
#
# File: macros/karmarkar.sci, line: 28
# File: macros/karmarkar.sci, line: 32
# File: macros/karmarkar.sci, line: 36
#, c-format
msgid "%s: Wrong size for input argument #%d."
msgstr ""
#
# File: macros/derivative.sci, line: 45
#, c-format
msgid "%s: Wrong size for input argument #%d: A column vector expected.\n"
msgstr ""
#
# File: sci_gateway/c/sci_qld.c, line: 62
#, c-format
msgid "%s: Wrong size for input argument #%d: number of columns %d expected.\n"
msgstr ""
#
# File: macros/derivative.sci, line: 173
#, c-format
msgid "%s: Wrong type for input argument #%d: A function expected.\n"
msgstr ""
#
# File: macros/derivative.sci, line: 41
#, c-format
msgid "%s: Wrong type for input argument #%d: N-dimensionnal array expected.\n"
msgstr ""
#
# File: macros/bvodeS.sci, line: 42
# File: macros/bvodeS.sci, line: 51
# File: macros/bvodeS.sci, line: 59
#, c-format
msgid "%s: Wrong type for input argument #%s: external expected.\n"
msgstr ""
#
# File: macros/karmarkar.sci, line: 39
#, c-format
msgid "%s: x0 is not feasible."
msgstr ""
#
# File: src/c/sp.c, line: 526
#, c-format
msgid "Error in dgels, info = %d.\n"
msgstr ""
#
# File: src/c/sp.c, line: 594
#, c-format
msgid "Error in dspev, info = %d.\n"
msgstr ""
#
# File: src/c/sp.c, line: 428
# File: src/c/sp.c, line: 613
#, c-format
msgid "Error in dspgv, info = %d.\n"
msgstr ""
#
# File: src/c/sp.c, line: 586
#, c-format
msgid "Error in dspst, info = %d.\n"
msgstr ""
#
# File: src/c/sp.c, line: 538
#, c-format
msgid "Error in dtrcon, info = %d.\n"
msgstr ""
#
# File: src/c/sp.c, line: 446
msgid "F(x)*Z has a negative eigenvalue.\n"
msgstr ""
#
# File: src/c/sp.c, line: 293
msgid "L must be at least one.\n"
msgstr ""
#
# File: src/c/sp.c, line: 324
msgid "Matrices Fi, i=1,...,m are linearly dependent.\n"
msgstr ""
#
# File: etc/optimization.start, line: 27
msgid "Optimization and Simulation"
msgstr ""
#
# File: src/c/sp.c, line: 542
msgid ""
"The matrices F_i, i=1,...,m are linearly dependent (or the initial points "
"are very badly conditioned).\n"
msgstr ""
#
# File: src/c/sp.c, line: 373
#, c-format
msgid "Work space is too small.  Need at least %d*sizeof(double).\n"
msgstr ""
#
# File: src/c/sp.c, line: 338
msgid "Z0 does not satisfy equality conditions for dual feasibility.\n"
msgstr ""
#
# File: src/c/sp.c, line: 443
msgid "Z0 is not positive definite.\n"
msgstr ""
#
# File: src/c/sp.c, line: 298
#, c-format
msgid "blck_szs[%d] must be at least one.\n"
msgstr ""
#
# File: src/c/sp.c, line: 288
msgid "m must be at least one.\n"
msgstr ""
#
# File: src/c/sp.c, line: 303
msgid "nu must be at least 1.0.\n"
msgstr ""
#
# File: src/c/sp.c, line: 430
msgid "x0 is not strictly primal feasible.\n"
msgstr ""
