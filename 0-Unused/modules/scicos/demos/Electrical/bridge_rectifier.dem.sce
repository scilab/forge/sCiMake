//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA
//
// This file is distributed under the same license as the Scilab package.
//

thispath = get_absolute_file_path("bridge_rectifier.dem.sce");
xcos(thispath+"/Bridge_Rectifier.cosf");
clear thispath;
