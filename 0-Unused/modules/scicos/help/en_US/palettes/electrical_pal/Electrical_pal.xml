<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="Electrical_pal">
  <refnamediv>
    <refname>Electrical_pal</refname>
    <refpurpose>Electrical toolbox</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/Electrical_pal_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Module_Electrical_pal">
    <title>Module</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="scicos_manual">Scicos</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_Electrical_pal">
    <title>Description</title>
    <para>
Electrical toolbox contains very basic electrical components
such as voltage source, diode, capacitor, etc.

</para>
  </refsection>
  <refsection id="Blocks_Electrical_pal">
    <title>Blocks</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Capacitor">Capacitor - Electrical capacitor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="CCS">CCS - Controllable Modelica current source</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="ConstantVoltage">ConstantVoltage - Electrical DC voltage source</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="CurrentSensor">CurrentSensor - Electrical current sensor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="CVS">CVS - Controllable Modelica voltage source</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="Diode">Diode - Electrical diode</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="Ground">Ground - Ground (zero potential reference)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="Gyrator">Gyrator - Modelica Gyrator</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="IdealTransformer">IdealTransformer - Ideal Transformer</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="Inductor">Inductor - Electrical inductor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="NMOS">NMOS - Simple NMOS Transistor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="NPN">NPN - NPN transistor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="OpAmp">OpAmp - Ideal opamp (norator-nullator pair)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="PMOS">PMOS - Simple PMOS Transistor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="PNP">PNP - PNP transistor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="PotentialSensor">PotentialSensor - Potential sensor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="Resistor">Resistor - Electrical resistor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="SineVoltage">SineVoltage - Sine voltage source</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="Switch">Switch - Non-ideal electrical switch</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="VariableResistor">VariableResistor - Electrical variable resistor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="VoltageSensor">VoltageSensor - Electrical voltage sensor</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="VsourceAC">VsourceAC - Electrical AC voltage source</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="VVsourceAC">VVsourceAC - Variable AC voltage source</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
