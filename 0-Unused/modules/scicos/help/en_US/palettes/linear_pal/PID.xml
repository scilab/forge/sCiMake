<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="PID">
  <refnamediv>
    <refname>PID</refname>
    <refpurpose>PID regulator</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/PID_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_PID">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="PID">PID regulator</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_PID">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_PID">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_PID">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_PID">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_PID">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <link linkend="PID">Compiled Super Block content</link>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Authors_PID">Authors</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_PID">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Linear_pal">Linear - Linear palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_PID">
    <title>Description</title>
    <para>
This block implements a PID controller. The PID controller calculation (algorithm) involves three separate parameters; the Proportional, the Integral  and Derivative values. The Proportional value determines the reaction to the current error, the Integral determines the reaction based on the sum of recent errors and the Derivative determines the reaction to the rate at which the error has been changing. The weighted sum of these three actions is used to adjust the process via a control element such as the position of a control valve or the power supply of a heating element.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Dialogbox_PID">
    <title>Dialog box</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/PID_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Proportional</emphasis>
        </para>
        <para> The value of the gain that multiply the error.</para>
        <para> Properties : Type 'vec' of size -1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Integral</emphasis>
        </para>
        <para> The value of the integral time of the error.(1/Integral)</para>
        <para> Properties : Type 'vec' of size -1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Derivation</emphasis>
        </para>
        <para> The value of the derivative time of the error.</para>
        <para> Properties : Type 'vec' of size -1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Defaultproperties_PID">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">name of computational function:</emphasis>
          <emphasis role="italic">csuper</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_PID">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Linear/PID.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="CompiledSuperBlockcontent_PID">
    <title>Compiled Super Block content</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/PID_img3_eng.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Authors_PID">
    <title>Authors</title>
    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
