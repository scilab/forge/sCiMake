<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="DLSS">
  <refnamediv>
    <refname>DLSS</refname>
    <refpurpose>Système d'équations d'état discrètes</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/DLSS_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_DLSS">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="DLSS">Système d'équations d'état discrètes</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_DLSS">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_DLSS">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_DLSS">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_DLSS">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_DLSS">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_DLSS">Fonction de calcul</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_DLSS">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Linear_pal">Linear - Palette Lineaire</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_DLSS">
    <title>Description</title>
    <para>
Ce bloc réalise un système d'équations d'état temporel discret.
Le système est défini par les matrices 
et par l'état initial .
Les dimensions des matrices et de l'état initial doivent être appropriées.
Lorsque qu'un événement active le bloc (par son unique port événementiel)
l'état est alors mis à jour.

</para>
  </refsection>
  <refsection id="Boîtededialogue_DLSS">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/DLSS_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold"><inlinemediaobject><imageobject><imagedata fileref="../../../images/DLSS_img5_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> matrix</emphasis>
        </para>
        <para> Une matrice carré.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">B matrix</emphasis>
        </para>
        <para> La matrice<inlinemediaobject><imageobject><imagedata fileref="../../../images/DLSS_img6_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> , [] si le système n'a pas d'entrées.</para>
        <para> Propriétés : Type 'mat' de taille ["size(%1,2)","-1"]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">C matrix</emphasis>
        </para>
        <para> La matrice<inlinemediaobject><imageobject><imagedata fileref="../../../images/DLSS_img7_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> , [] si le système n'a pas de sorties.</para>
        <para> Propriétés : Type 'mat' de taille ["-1","size(%1,2)"]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">D matrix</emphasis>
        </para>
        <para> La matrice<inlinemediaobject><imageobject><imagedata fileref="../../../images/DLSS_img8_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> , [] si le système n'a pas de terme D.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Initial state</emphasis>
        </para>
        <para> Un état initial du système ( vectoriel ou scalaire).</para>
        <para> Propriétés : Type 'vec' de taille "size(%1,2)". </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_DLSS">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">dsslti4</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_DLSS">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Linear/DLSS.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_DLSS">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/dsslti4.c (Type 4)</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
