<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="CUMSUM">
  <refnamediv>
    <refname>CUMSUM</refname>
    <refpurpose>CUMSUM Somme cumulative</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/CUMSUM_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_CUMSUM">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="CUMSUM">CUMSUM Somme cumulative</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_CUMSUM">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_CUMSUM">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_CUMSUM">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_CUMSUM">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_CUMSUM">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_CUMSUM">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_CUMSUM">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_CUMSUM">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_CUMSUM">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Matrix_pal">Matrix - Palette d'opérations matricielles</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_CUMSUM">
    <title>Description</title>
    <para>
The CUMSUM block sums the element of an input m*n matrix u along either the rows, the columns or the first non singleton dimension. When the "Sum along" parameter is set to "1", the block sums across the elements of each row. The result will be displayed as a m*1 matrix.  
</para>
    <para>
When the "Sum along" parameter is set to "2", the block sums across the elements of each column. The result will be display as a 1*n matrix.  
</para>
    <para>
When the "Sum along" parameter is set to "0", the block sums across the first non singleton dimension. The result will be displayed as one element. This block is equivalent to cumsum in scilab.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_CUMSUM">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/CUMSUM_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Datatype(1=real double 2=Complex)</emphasis>
        </para>
        <para> It indicates the type of the output. It support only the two types double (1) and complex (2). If we input another entry in this label scicos will print the message "Datatype is not supported".</para>
        <para> Propriétés : Type 'vec' de taille 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Sum along (0=the first non singleton dimension 1=Rows 2=Columns)</emphasis>
        </para>
        <para> Indicate whether to sum across the rows, the columns or the first non singleton dimension.</para>
        <para> Propriétés : Type 'vec' de taille 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_CUMSUM">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">cumsum_m</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_CUMSUM">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/MatrixOp/CUMSUM.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_CUMSUM">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cumsum_m.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cumsum_r.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cumsum_c.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cumsumz_m.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cumsumz_r.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cumsumz_c.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_CUMSUM">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATSUM">MATSUM - Somme matricielle (Bloc Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_CUMSUM">
    <title>Auteurs</title>
    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
