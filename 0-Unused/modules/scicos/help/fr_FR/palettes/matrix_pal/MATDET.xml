<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="MATDET">
  <refnamediv>
    <refname>MATDET</refname>
    <refpurpose>MATDET Déterminant</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/MATDET_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_MATDET">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATDET">MATDET Déterminant</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_MATDET">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_MATDET">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_MATDET">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_MATDET">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_MATDET">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_MATDET">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_MATDET">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_MATDET">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_MATDET">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Matrix_pal">Matrix - Palette d'opérations matricielles</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_MATDET">
    <title>Description</title>
    <para>
The MATDET outputs the determinant of a square input matrix. If the input A=[A11 A12 A13;A21 A22 A23;A31 A32 A33] then the output of the block has the form of: y=A11*(A22*A33-A23*A32)-A12*(A21*A33-A23*A31)+A13*(A21*A32-A22*A31).  
</para>
    <para>
The equivalent of MATDET in Scilab is "det"  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_MATDET">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/MATDET_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Datatype(1=real double 2=Complex)</emphasis>
        </para>
        <para> Ce paramètre indique le type de donnée de la sortie. Ce bloc fonctionne uniquement avec des types de donnée réels(1) et complexes(2). Si un autre type que 1 et 2 est indiqué, alors Scicos retourne le message d'erreur "Datatype is not supported".</para>
        <para> Propriétés : Type 'vec' de taille 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_MATDET">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-1,-1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">mat_det</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_MATDET">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/MatrixOp/MATDET.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_MATDET">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/mat_det.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/matz_det.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_MATDET">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATINV">MATINV - MATINV Matrice inverse (Bloc Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_MATDET">
    <title>Auteurs</title>
    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
