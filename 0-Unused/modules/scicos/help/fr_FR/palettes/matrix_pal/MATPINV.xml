<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="MATPINV">
  <refnamediv>
    <refname>MATPINV</refname>
    <refpurpose>MATPINV Pseudo-invertion matricielle</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/MATPINV_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_MATPINV">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATPINV">MATPINV Pseudo-invertion matricielle</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_MATPINV">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_MATPINV">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_MATPINV">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_MATPINV">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_MATPINV">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_MATPINV">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_MATPINV">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_MATPINV">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_MATPINV">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Matrix_pal">Matrix - Palette d'opérations matricielles</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_MATPINV">
    <title>Description</title>
    <para>
The MATPINV Block outputs the inverse of a non square input matrix using the SVD theory.if the SVD decomposition of A is equal to:  
</para>
    <para>
A=USV' 
</para>
    <para>
The pseudoinverse x of A is given by:  
</para>
    <para>
X=VS"U' where S"(i,j)=1/S(i,j) (if S(i,j) =0), U' and V are respectivly the transpose of U and V'.  
</para>
    <para>
and we have A*X*A=A and X*A*X=X. Both A*X and X*A are Hermitian . A warning message is printed if the input is badly scaled or nearly singular.  
</para>
    <para>
When the input is a M-by-N matrix the output is a N-by-M matrix. The eqivalent function of this block in Scilab is "pinv".  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_MATPINV">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/MATPINV_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Datatype(1=real double 2=Complex)</emphasis>
        </para>
        <para> Ce paramètre indique le type de donnée de la sortie. Ce bloc fonctionne uniquement avec des types de donnée réels(1) et complexes(2). Si un autre type que 1 et 2 est indiqué, alors Scicos retourne le message d'erreur "Datatype is not supported".</para>
        <para> Propriétés : Type 'vec' de taille 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_MATPINV">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-2,-1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">mat_pinv</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_MATPINV">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/MatrixOp/MATPINV.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_MATPINV">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/mat_pinv.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/matz_pinv.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_MATPINV">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATINV">MATINV - MATINV Matrice inverse (Bloc Scicos)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="MATSING">MATSING - MATSING Décomposition en valeurs singulières (Bloc Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_MATPINV">
    <title>Auteurs</title>
    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
