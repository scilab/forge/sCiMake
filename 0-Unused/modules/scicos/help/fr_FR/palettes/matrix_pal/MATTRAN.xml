<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="MATTRAN">
  <refnamediv>
    <refname>MATTRAN</refname>
    <refpurpose>MATTRAN Transposition Matricielle</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/MATTRAN_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_MATTRAN">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATTRAN">MATTRAN Transposition Matricielle</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_MATTRAN">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_MATTRAN">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_MATTRAN">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <link linkend="MATTRAN">Exemple</link>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_MATTRAN">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_MATTRAN">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_MATTRAN">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_MATTRAN">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_MATTRAN">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_MATTRAN">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Matrix_pal">Matrix - Palette d'opérations matricielles</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_MATTRAN">
    <title>Description</title>
    <para>
Ce bloc fait la transposition d'une matrice de taille MxN en matrice de taille NxM. Pour les données de type complexe, celui-ci utilise une transposition Hermitienne. L'instruction scilab équivalente de ce bloc est y=u'.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_MATTRAN">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/MATTRAN_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Datatype(1=real double 2=Complex)</emphasis>
        </para>
        <para> Ce paramètre indique le type de donnée de la sortie. Ce bloc fonctionne uniquement avec des types de donnée réels(1) et complexes(2). Si un autre type que 1 et 2 est indiqué, alors Scicos retourne le message d'erreur "Datatype is not supported".</para>
        <para> Propriétés : Type 'vec' de taille 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Exemple_MATTRAN">
    <title>Exemple</title>
    <programlisting role="example"><![CDATA[ 
.                     Transposition 
--                  --               --           --
| 1+i   2+3i    3+2i |   Complexe    | 1- i   4- i |
| 4+i   5-8i    6-2i | ----------->  | 2-3i   5+8i |
--                  --               | 3-2i   6+2i |
.                                    --           --
.                  Reel 
--          --               --      --
| 1   -2   3 | Tranposition  | 1    4 |
| 4   5    6 | ------------> |- 2   5 |
--          --               | 3    6 |
.                            --      --
 ]]></programlisting>
  </refsection>
  <refsection id="Propriétéspardéfaut_MATTRAN">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [-2,-1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">mattran_m</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_MATTRAN">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/MatrixOp/MATTRAN.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_MATTRAN">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/mattran_m.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/matztran_m.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_MATTRAN">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Matrix_pal">Matrix_pal - Palette d'opérations matricielles (Palette Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_MATTRAN">
    <title>Auteurs</title>
    <para>
      <emphasis role="bold"/>
    </para>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Fady NASSIF</emphasis> INRIA</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Alan Layec</emphasis> INRIA</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
