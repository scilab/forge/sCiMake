<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="scifunc_block_m">
  <refnamediv>
    <refname>scifunc_block_m</refname>
    <refpurpose>Bloc Scilab</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/scifunc_block_m_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_scifunc_block_m">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="scifunc_block_m">Bloc Scilab</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_scifunc_block_m">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_scifunc_block_m">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_scifunc_block_m">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_scifunc_block_m">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_scifunc_block_m">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_scifunc_block_m">Fonction de calcul</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_scifunc_block_m">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Others_pal">Others - Palette Others</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_scifunc_block_m">
    <title>Description</title>
    <para>
This block can realize any type of Scicos block. The function of the
block is defined interactively using dialogue boxes and in Scilab
language. During simulation, these instructions are interpreted by
Scilab; the simulation of diagrams that include these types of blocks
is slower. For more information see Scicos reference manual. 

</para>
  </refsection>
  <refsection id="Boîtededialogue_scifunc_block_m">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/scifunc_block_m_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">input ports sizes</emphasis>
        </para>
        <para> Un scalaire. Le nombre de ports réguliers d'entrée.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">output port sizes</emphasis>
        </para>
        <para> Un scalaire. Le nombre de ports réguliers de sortie.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">input event ports sizes</emphasis>
        </para>
        <para> Un scalaire. Le nombre de ports événementiel d'entrée.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">output events ports sizes</emphasis>
        </para>
        <para> Un scalaire. Le nombre de ports événementiel de sortie.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">initial continuous state</emphasis>
        </para>
        <para> Un vecteur colonne. Conditions initiales des états continus.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">initial discrete state</emphasis>
        </para>
        <para> Un vecteur colonne. Conditions initiales des états discrets.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">System parameters vector</emphasis>
        </para>
        <para> une chaîne de caractères : c ou d (<emphasis role="bold">CBB</emphasis> ou<emphasis role="bold">DBB</emphasis> ), les autres types ne sont supportés.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">initial firing vector </emphasis>
        </para>
        <para> Un vecteur. La taille de ce vecteur correspond au nombre de sorties événementielles. La valeur de la <inlinemediaobject><imageobject><imagedata fileref="../../../images/scifunc_block_m_img3_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> entrée spécifie la date initiale préprogrammée du <inlinemediaobject><imageobject><imagedata fileref="../../../images/scifunc_block_m_img3_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> port événementiel de sortie. Si la valeur est inférieure à zéro, alors aucun événement n'est préprogrammé.</para>
        <para> Propriétés : Type 'vec' de taille sum(%4) </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">is block always active </emphasis>
        </para>
        <para> other dialogues are opened consecutively where used may input Scilab code associated with the computations needed (block initialization, outputs, continuous and discrete state, output events date, block ending)</para>
        <para> Propriétés : Type 'vec' de taille 1</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_scifunc_block_m">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">scifunc</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_scifunc_block_m">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Misc/scifunc_block_m.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_scifunc_block_m">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos/src/fortran/scifunc.f (Type 3)</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
