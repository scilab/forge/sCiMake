<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="AFFICH_m">
  <refnamediv>
    <refname>AFFICH_m</refname>
    <refpurpose>Affichage</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/AFFICH_m_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_AFFICH_m">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="AFFICH_m">Affichage</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_AFFICH_m">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_AFFICH_m">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_AFFICH_m">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_AFFICH_m">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_AFFICH_m">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_AFFICH_m">Fonction de calcul</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_AFFICH_m">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Sinks_pal">Sinks - Palette Affichage</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_AFFICH_m">
    <title>Description</title>
    <para>
Pendant la simulation, ce bloc affiche sur le diagramme 
la valeur lue sur son unique port d'entrée.

</para>
  </refsection>
  <refsection id="Boîtededialogue_AFFICH_m">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/AFFICH_m_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Input Size</emphasis>
        </para>
        <para> Définit explicitement la taille du port d'entrée.</para>
        <para> Propriétés : Type 'mat' de taille [1,2]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Font number</emphasis>
        </para>
        <para> Entier, la taille de la fonte sélectionnée (voir xset).</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Font size</emphasis>
        </para>
        <para> Entier, la taille de la police utilisée.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Color</emphasis>
        </para>
        <para> Entier, la couleur sélectionnée pour le texte. (voir xset)</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Total number of digits</emphasis>
        </para>
        <para> Un entier plus grand que 3. C'est le nombre de chiffres utilisés pour représenter le nombre affiché. (incluant le signe, la partie enière et les chiffres après la virgule)</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Number of rational part digits</emphasis>
        </para>
        <para> Un entier plus grand ou égal à 0. C'est le nombre de chiffres affichés après la virgule.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Block inherits </emphasis>
        </para>
        <para> Option pour choisir l'activation par héritage d'événement via le port d'entrée régulier ou explicitement par le port d'entrée événementiel.</para>
        <para> Propriétés : Type 'vec' de taille 1</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_AFFICH_m">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">affich2</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_AFFICH_m">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Sinks/AFFICH_m.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_AFFICH_m">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/fortran/affich2.f (Type 0)</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
