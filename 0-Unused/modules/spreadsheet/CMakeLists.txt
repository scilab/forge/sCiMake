INCLUDE_DIRECTORIES(
  includes/ 
  src/c/ 
  src/c/ripole 
  ../../libs/MALLOC/includes/ 
  ../../modules/output_stream/includes 
  ../../modules/io/includes 
  ../../modules/api_scilab/includes 
  ../../modules/fileio/includes
) 

FILE(GLOB
  SPREADSHEET_C_SOURCES
  src/c/xls.c
)

FILE(GLOB
  RIPOLE_SOURCES
  src/c/ripole/olestream-unwrap.c 
  src/c/ripole/pldstr.c 
  src/c/ripole/ole.c 
  src/c/ripole/bytedecoders.c 
  src/c/ripole/logger.c 
  src/c/ripole/ripole.c 
  src/c/ripole/bt-int.c
)


FILE(GLOB
  GATEWAY_C_SOURCES
  sci_gateway/c/gw_spreadsheet.c 
  sci_gateway/c/sci_xls_read.c 
  sci_gateway/c/sci_xls_open.c
)

ADD_LIBRARY(spreadsheet
  ${SPREADSHEET_C_SOURCES}
  ${GATEWAY_C_SOURCES}
  ${RIPOLE_SOURCES}
)

TARGET_LINK_LIBRARIES(spreadsheet
  fileio
  MALLOC
  output_stream
  core
)