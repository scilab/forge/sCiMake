<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="MATZREIM">
  <refnamediv>
    <refname>MATZREIM</refname>
    <refpurpose>Complex decomposition</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../../images/palettes/MATZREIM.jpg" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_MATZREIM">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATZREIM">Complex decomposition</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_MATZREIM">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_MATZREIM">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_MATZREIM">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_MATZREIM">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_MATZREIM">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_MATZREIM">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Seealso_MATZREIM">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Authors_MATZREIM">Authors</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_MATZREIM">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Mathoperations_pal">Math operations palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_MATZREIM">
    <title>Description</title>
    <para>
This block decomposes a complex number by separating the real and imaginary parts or compose a complex number by joining the two parts. The user can select even to separate or to join real and imaginary part by setting the decomposition type to 1 or 2. When it is set to 1, the input is a complex matrix and the outputs are the real and imaginary parts of the input. When it set to 2, The inputs are two real matrices, the output is a complex number with real part the first input and imaginary part the second input.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Dialogbox_MATZREIM">
    <title>Dialog box</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../../images/gui/MATZREIM_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">decomposition type (1=Complex2Real&amp;amp;Imag 2=Real&amp;amp;Imag2Complex)</emphasis>
        </para>
        <para> Indicates the type to use for the decomposition. See the description part for more information.</para>
        <para> Properties : Type 'vec' of size 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Defaultproperties_MATZREIM">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> yes</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 2</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 2 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">name of computational function:</emphasis>
          <emphasis role="italic">matz_reim</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_MATZREIM">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/MatrixOp/MATZREIM.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_MATZREIM">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/matz_reim.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/matz_reimc.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Seealso_MATZREIM">
    <title>See also</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="MATMAGPHI">MATMAGPHI - MATMAGPHI Complex to Magnitude and Angle Conversion</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Authors_MATZREIM">
    <title>Authors</title>
    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
