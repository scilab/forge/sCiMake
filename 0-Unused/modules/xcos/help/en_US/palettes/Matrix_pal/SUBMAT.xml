<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry version="5.0-subset Scilab" xml:id="SUBMAT"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>SUBMAT</refname>

    <refpurpose>Sub-matrix extraction</refpurpose>
  </refnamediv>

  <refsection>
    <title>Block Screenshot</title>

    <para><inlinemediaobject>
        <imageobject>
          <imagedata align="center"
                     fileref="../../../../images/palettes/SUBMAT.jpg"
                     valign="middle"></imagedata>
        </imageobject>
      </inlinemediaobject></para>
  </refsection>

  <refsection id="Contents_SUBMAT">
    <title>Contents</title>

    <itemizedlist>
      <listitem>
        <para>
          <link linkend="SUBMAT">Sub-matrix extraction</link>
        </para>
      </listitem>

      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_SUBMAT">Palette</xref>
            </para>
          </listitem>

          <listitem>
            <para>
              <xref linkend="Description_SUBMAT">Description</xref>
            </para>
          </listitem>

          <listitem>
            <para>
              <xref linkend="Dialogbox_SUBMAT">Dialog box</xref>
            </para>
          </listitem>

          <listitem>
            <para>
              <xref linkend="Defaultproperties_SUBMAT">Default
              properties</xref>
            </para>
          </listitem>

          <listitem>
            <para>
              <xref linkend="Interfacingfunction_SUBMAT">Interfacing
              function</xref>
            </para>
          </listitem>

          <listitem>
            <para>
              <xref linkend="Computationalfunction_SUBMAT">Computational
              function</xref>
            </para>
          </listitem>

          <listitem>
            <para>
              <xref linkend="Authors_SUBMAT">Authors</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection id="Palette_SUBMAT">
    <title>Palette</title>

    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Matrix_pal">Matrix operation palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection id="Description_SUBMAT">
    <title>Description</title>

    <para>This block outputs a sub matrix of the input matrix. The output
    matrix will be defining by using the parameters of this block.</para>

    <para />
  </refsection>

  <refsection id="Dialogbox_SUBMAT">
    <title>Dialog box</title>

    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center"
                     fileref="../../../../images/gui/SUBMAT_gui.gif"
                     valign="middle" />
        </imageobject>
      </inlinemediaobject>
    </para>

    <para />

    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Datatype (1=real double 2=Complex)</emphasis>
        </para>

        <para>Type of the output matrix. It can be double or complex.</para>

        <para>Properties : Type 'vec' of size 1.</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">Starting Row Index</emphasis>
        </para>

        <para>The first row of the submatrix.</para>

        <para>Properties : Type 'vec' of size 1.</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">Ending Row Index</emphasis>
        </para>

        <para>The last row of the Submatrix.</para>

        <para>Properties : Type 'vec' of size 1.</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">Starting Column Index</emphasis>
        </para>

        <para>The first column of the submatrix.</para>

        <para>Properties : Type 'vec' of size 1.</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">Ending Column Index</emphasis>
        </para>

        <para>The last row of the submatrix.</para>

        <para>Properties : Type 'vec' of size 1.</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">Input Dimension</emphasis>
        </para>

        <para>The Matrix input dimensions.</para>

        <para>Properties : Type 'vec' of size 2.</para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection id="Defaultproperties_SUBMAT">
    <title>Default properties</title>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> yes</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>

        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>

        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">number/sizes of activation
        inputs:</emphasis> 0</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">number/sizes of activation
        outputs:</emphasis> 0</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis>
        no</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis>
        no</para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">name of computational function:</emphasis>

          <emphasis role="italic">submat</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection id="Interfacingfunction_SUBMAT">
    <title>Interfacing function</title>

    <itemizedlist>
      <listitem>
        <para>SCI/modules/scicos_blocks/macros/MatrixOp/SUBMAT.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection id="Computationalfunction_SUBMAT">
    <title>Computational function</title>

    <itemizedlist>
      <listitem>
        <para>SCI/modules/scicos_blocks/src/c/submat.c</para>
      </listitem>

      <listitem>
        <para>SCI/modules/scicos_blocks/src/c/submatz.c</para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection id="Authors_SUBMAT">
    <title>Authors</title>

    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
