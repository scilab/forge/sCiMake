# Determine the compiler to use for OCAML programs
# NOTE, a generator may set CMAKE_OCAML_COMPILER before
# loading this file to force a compiler.
# use environment variable OCAML first if defined by user, next use 
# the cmake variable CMAKE_GENERATOR_OCAML which can be defined by a generator
# as a default compiler

IF(NOT CMAKE_OCAML_COMPILER)

  # prefer the environment variable OCAML
  IF($ENV{OCAML} MATCHES ".+")
    GET_FILENAME_COMPONENT(CMAKE_OCAML_COMPILER_INIT $ENV{OCAML} PROGRAM PROGRAM_ARGS CMAKE_OCAML_FLAGS_ENV_INIT)
    IF(CMAKE_OCAML_FLAGS_ENV_INIT)
      SET(CMAKE_OCAML_COMPILER_ARG1 "${CMAKE_OCAML_FLAGS_ENV_INIT}" CACHE STRING "First argument to OCAML compiler")
    ENDIF(CMAKE_OCAML_FLAGS_ENV_INIT)
    IF(EXISTS ${CMAKE_OCAML_COMPILER_INIT})
    ELSE(EXISTS ${CMAKE_OCAML_COMPILER_INIT})
      MESSAGE(FATAL_ERROR "Could not find compiler set in environment variable OCAML:\n$ENV{OCAML}.") 
    ENDIF(EXISTS ${CMAKE_OCAML_COMPILER_INIT})
  ENDIF($ENV{OCAML} MATCHES ".+")

  # next try prefer the compiler specified by the generator
  IF(CMAKE_GENERATOR_OCAML) 
    IF(NOT CMAKE_OCAML_COMPILER_INIT)
      SET(CMAKE_OCAML_COMPILER_INIT ${CMAKE_GENERATOR_OCAML})
    ENDIF(NOT CMAKE_OCAML_COMPILER_INIT)
  ENDIF(CMAKE_GENERATOR_OCAML)

  # finally list compilers to try
  IF(CMAKE_OCAML_COMPILER_INIT)
    SET(CMAKE_OCAML_COMPILER_LIST ${CMAKE_OCAML_COMPILER_INIT})
  ELSE(CMAKE_OCAML_COMPILER_INIT)
    SET(CMAKE_OCAML_COMPILER_LIST ocamlopt)  
  ENDIF(CMAKE_OCAML_COMPILER_INIT)

  # Find the compiler.
  FIND_PROGRAM(CMAKE_OCAML_COMPILER NAMES ${CMAKE_OCAML_COMPILER_LIST} DOC "OCAML compiler")
  IF(CMAKE_OCAML_COMPILER_INIT AND NOT CMAKE_OCAML_COMPILER)
    SET(CMAKE_OCAML_COMPILER "${CMAKE_OCAML_COMPILER_INIT}" CACHE FILEPATH "OCAML compiler" FORCE)
  ENDIF(CMAKE_OCAML_COMPILER_INIT AND NOT CMAKE_OCAML_COMPILER)
ENDIF(NOT CMAKE_OCAML_COMPILER)
MARK_AS_ADVANCED(CMAKE_OCAML_COMPILER)

GET_FILENAME_COMPONENT(COMPILER_LOCATION "${CMAKE_OCAML_COMPILER}"
  PATH)

#FIND_PROGRAM(GNAT_EXECUTABLE_BUILDER NAMES gnatmake PATHS ${COMPILER_LOCATION} )
FIND_PROGRAM(CMAKE_AR NAMES ar PATHS ${COMPILER_LOCATION} )

FIND_PROGRAM(CMAKE_RANLIB NAMES ranlib)
IF(NOT CMAKE_RANLIB)
   SET(CMAKE_RANLIB : CACHE INTERNAL "noop for ranlib")
ENDIF(NOT CMAKE_RANLIB)
MARK_AS_ADVANCED(CMAKE_RANLIB)

# configure variables set in this file for fast reload later on
#CONFIGURE_FILE(${CMAKE_ROOT}/Modules/CMakeOCAMLCompiler.cmake.in 
CONFIGURE_FILE(${CMAKE_MODULE_PATH}/CMakeOCAMLCompiler.cmake.in 
               "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeOCAMLCompiler.cmake" IMMEDIATE)
MARK_AS_ADVANCED(CMAKE_AR)

SET(CMAKE_OCAML_COMPILER_ENV_VAR "OCAML")
