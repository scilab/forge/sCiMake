include(FindPackageHandleStandardArgs)

FIND_PROGRAM(ANT_FOUND
  NAMES "ant"
  PATH /usr/bin
  DOC "Ant - build generator for Java"
)

find_package_handle_standard_args(ANT DEFAULT_MSG ANT_FOUND)


MACRO(USEANT ARG1)
  add_custom_target(${ARG1}-javalibs ALL
    COMMAND "cp" "-r" "${CMAKE_CURRENT_SOURCE_DIR}/src/" "${CMAKE_CURRENT_BINARY_DIR}/"
    COMMAND "cp" "-r" "${CMAKE_CURRENT_SOURCE_DIR}/build.xml" "${CMAKE_CURRENT_BINARY_DIR}/build.xml"
    COMMAND "/usr/bin/ant" "-f" "${CMAKE_CURRENT_BINARY_DIR}/build.xml"
  )
ENDMACRO()

LIST(APPEND DEFAULT_JAR_DIRS
  /usr/share/java/ 
  /usr/lib/java/ 
  /usr/share/java 
  /usr/share/java/jar 
  /opt/java/lib 
  /usr/local/java 
  /usr/local/java/jar 
  /usr/local/share/java 
  /usr/local/share/java/jar 
  /usr/local/lib/java
)



MACRO(JAVA_TRY_COMPILE ARG1 ARG2)
  SET(JV_TEST_PROG "import ${ARG1} \;
  public class conftest{public static void main(String[] argv) {}}")
  FILE(WRITE "${CMAKE_CURRENT_BINARY_DIR}/conftest.java" ${JV_TEST_PROG})
  EXECUTE_PROCESS(COMMAND "javac" "-cp" "${ARG2}" "${CMAKE_CURRENT_BINARY_DIR}/conftest.java"
		  RESULT_VARIABLE TST
		  ERROR_QUIET)

  IF(NOT ${TST})
    MESSAGE(STATUS "Successfully imported ${ARG1}.")
  ELSE(NOT ${TST})
#    MESSAGE(FATAL_ERROR "Error importing ${ARG1}.")
  ENDIF(NOT ${TST})
ENDMACRO()

MACRO(JAVA_CHECK_PACKAGE ARG1 ARG2 ARG3)
#  IF(NOT ${${ARG1}_FOUND})
    FIND_PATH(${ARG1}_PATH 
    NAMES "${ARG1}.jar"
    PATHS ${DEFAULT_JAR_DIR}
    )
    JAVA_TRY_COMPILE(${ARG2} "${${ARG1}_PATH}/${ARG1}.jar")
    SET(${ARG3} "${${ARG1}_PATH}/${ARG1}.jar")
    SET(${ARG1}_FOUND True)
#  ENDIF(NOT ${${ARG1}_FOUND})
ENDMACRO()

