
FIND_PACKAGE(ANT REQUIRED)

JAVA_CHECK_PACKAGE(batik "org.apache.batik.parser.Parser")
JAVA_CHECK_PACKAGE(saxon "com.icl.saxon.Loader")
JAVA_CHECK_PACKAGE(fop "org.apache.fop.pdf.PDFInfo")
JAVA_CHECK_PACKAGE(jeuclid-core "net.sourceforge.jeuclid.LayoutContext")
JAVA_CHECK_PACKAGE(commons-io "org.apache.commons.io.output.CountingOutputStream")
JAVA_CHECK_PACKAGE(xmlgraphics-commons "org.apache.xmlgraphics.util.Service")
JAVA_CHECK_PACKAGE(xml-apis-ext "org.w3c.dom.svg.SVGDocument")
JAVA_CHECK_PACKAGE(avalon-framework "org.apache.avalon.framework.configuration.ConfigurationException") 
