
FILE(GLOB
  INC
  includes/*.h
)


DEFINE_INCLUDES(${INC})

FILE(GLOB
  API_SCILAB_CPP_SOURCES
  src/cpp/api_boolean.cpp 
  src/cpp/api_boolean_sparse.cpp 
  src/cpp/api_common.cpp 
  src/cpp/api_double.cpp 
  src/cpp/api_int.cpp 
  src/cpp/api_list.cpp
  src/cpp/api_poly.cpp 
  src/cpp/api_sparse.cpp 
  src/cpp/api_string.cpp 
  src/cpp/api_pointer.cpp 
  src/cpp/api_error.cpp
)

ADD_LIBRARY(api_scilab
  ${API_SCILAB_CPP_SOURCES}
)

TARGET_LINK_LIBRARIES(api_scilab
  core
)