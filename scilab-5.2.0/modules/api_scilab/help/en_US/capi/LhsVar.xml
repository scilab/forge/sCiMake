<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="LhsVar"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>LhsVar</refname>

    <refpurpose>a C gateway function which specifies which parameters created
    inside the C gateway will be returned as an output argument into
    Scilab.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>LhsVar(RankPos) = RankVar;</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>RankPos</term>

        <listitem>
          <para>as integer providing the rank of the output argument</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>RankVar</term>

        <listitem>
          <para>the rank of the parameter created inside the C gateway to be
          returned as an Scilab output argument</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>A C gateway function which specifies which variables created inside
    the C interface will be returned as an output argumen into Scilab.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>This example takes a matrix of doubles as input and returns:</para>

    <itemizedlist>
      <listitem>
        <para>the number of lines (first output argument)</para>
      </listitem>

      <listitem>
        <para>the number of rows (second output argument)</para>
      </listitem>
    </itemizedlist>

    <para>We create an intermediate Scilab parameter which will handle an
    integer but will neither be used nor returned as an output
    argument.</para>

    <para>TODO: insert an example in the Scilab language</para>

    <programlisting role="example"><![CDATA[ 
#include <stack-c.h>

int sci_mysizedouble(char * fname)
{
  int m_in_row,          n_in_col,          l_in_pos;
  int m_out_lines_row,   n_out_lines_col,   l_out_lines_pos;
  int m_out_columns_row, n_out_columns_col, l_out_columns_pos;
  int m_nop,             n_nop,             l_nop;
  
  GetRhsVar(1, MATRIX_OF_DOUBLE_DATATYPE, &m_in_row, &n_in_col, &l_in_pos);

  m_out_lines_row   = 1; n_out_lines_col   = 1; // We create a scalar
  m_out_columns_row = 1; n_out_columns_col = 1; // We create a scalar
  m_nop             = 1; n_nop             = 1; // We create a scalar

  CreateVar(2, MATRIX_OF_INTEGER_DATATYPE, &m_out_lines_row,   &n_out_lines_col,   &l_out_lines_pos);
  CreateVar(3, MATRIX_OF_INTEGER_DATATYPE, &m_nop,             &n_nop,             &l_nop);
  CreateVar(4, MATRIX_OF_INTEGER_DATATYPE, &m_out_columns_row, &n_out_columns_col, &l_out_columns_pos);

  *istk(l_out_lines_pos)   = m_in_row; // the out_lines_pos parameter handles the number of lines of the matrix sent as argument
  *istk(l_nop)             = 1; // store a mere value, but will neither be used nor returned to Scilab
  *istk(l_out_columns_pos) = n_in_col; // the out_columns_pos parameter handles the number of columns of the matrix sent as argument

  LhsVar(1) = 2; // We set the parameter 2 as an output argument
  LhsVar(2) = 4; // We set the parameter 4 as an output argument

  return 0;
}
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="ScilabCTypes">Scilab C Type</link></member>

      <member><link linkend="istk">istk</link></member>

      <member><link linkend="CreateVar">CreateVar</link></member>

      <member><link linkend="GetRhsVar">GetRhsVar</link></member>
    </simplelist>
  </refsection>
</refentry>
