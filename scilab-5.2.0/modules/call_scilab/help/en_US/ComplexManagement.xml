<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2009  - DIGITEO - Sylvestre LEDRU
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="ComplexManagement_callscilab"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>Complex management</refname>

    <refpurpose>How to manage Scilab's complex variable read and write process
    using call_scilab</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>This help describes how doubles and matrix of complex can be handle
    through the Call Scilab API.</para>

    <para>There are several functions which can be used to read / write from
    the memory of Scilab. These functions are described in dedicated
    pages.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
/* 
 * Write a matrix into Scilab
 * B=[1+%i 4-%i 2+1/2*%i 3; 
 *    3 9 8+42*%i 2 ]
 * Note that it is done column by column
 */ 

double B[]={1,3,4,9,2,8,3,2,1,0,-1,0,1/2,42,0,0};   /* Declare the matrix */
int rowB=2, colB=4; /* Size of the matrix */
char variableNameB[] = "B";

C2F(cwritecmat)(variableNameB, &rowB, &colB, B, strlen(variableNameB)); /* Write it into Scilab's memory */

printf("\n");
printf("Display from Scilab of B:\n");
SendScilabJob("disp(B);"); /* Display B */
 ]]></programlisting>

    <programlisting role="example"><![CDATA[ 
int rowB_ = 0, colB_ = 0, lp_ = 0;
int i = 0,j = 0;

double *matrixOfComplexB = NULL;
char variableToBeRetrievedB[] = "B";

/* First, retrieve the size of the matrix */
C2F(cmatcptr)(variableToBeRetrievedB, &rowB_, &colB_, &lp_, strlen(variableToBeRetrievedB));

/* Alloc the memory */
matrixOfComplexB = (double*)malloc((rowB_*colB_*2)*sizeof(double));

/* Load the matrix */
C2F(creadcmat)(variableToBeRetrievedB,&rowB_,&colB_,matrixOfComplexB,strlen(variableToBeRetrievedB) );

printf("\n");
printf("Display from B formated (size: %d, %d):\n",rowB_, colB_);
for(j = 0 ; j < rowB_ ; j++)
 {
  for(i = 0 ; i < colB_ ; i++)
   {
    /* Display the formated matrix ... the way the user
     * expect */
    printf("%5.2f + %5.2f.i  ",matrixOfComplexB[i * rowB_ + j],matrixOfComplexB[(rowB_*colB_)+(i * rowB_ + j)]);
   }
  printf("\n"); /* New row of the matrix */
 }
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="cwritecmat">cwritecmat</link>, <link
      linkend="creadcmat">creadcmat</link>, <link
      linkend="SendScilabJob">SendScilabJob</link>, <link
      linkend="StartScilab">StartScilab</link>, <link
      linkend="ComplexManagement_callscilab">Call_Scilab: Complex Management</link>, <link
      linkend="BooleanManagement_callscilab">Call_Scilab: Boolean Management</link>, <link
      linkend="StringManagement_callscilab">Call_Scilab: String Management</link>, <link
      linkend="boolean_reading_API">API_Scilab: Boolean Reading</link>, <link
      linkend="boolean_writing_API">API_Scilab: Boolean Writing</link>, <link
      linkend="String_management_reading_API">API_Scilab: String Reading</link>, <link
      linkend="String_management_writing_API">API_Scilab: String Writing</link>
	  </member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <para>Sylvestre Ledru</para>
  </refsection>
</refentry>
