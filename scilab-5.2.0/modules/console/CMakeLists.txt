FILE(GLOB
  INC
  includes/*.h
)

DEFINE_INCLUDES(${INC})

INCLUDE_DIRECTORIES(
  src/jni
  ${JNI_INCLUDE_DIRS} 
)

FILE(GLOB
  CONSOLE_CPP_JNI_SOURCES  
  src/jni/CallScilabBridge.cpp 
  src/jni/GiwsException.cpp
)

FILE(GLOB
  CONSOLE_CPP_SOURCES   
  src/cpp/ClearConsole.cpp 
  src/cpp/ClearConsolePart.cpp 
  src/cpp/ConsolePrintf.cpp 
  src/cpp/ConsoleRead.cpp 
  src/cpp/ConsoleIsWaitingForInput.cpp 
  src/cpp/GetCharWithoutOutput.cpp 
  src/cpp/PromptToHome.cpp 
  src/cpp/ScilabLinesUpdate.cpp 
  src/cpp/SetConsolePrompt.cpp
)


FILE(GLOB
  CONSOLE_C_JNI_SOURCES 
  src/jni/GuiManagement_wrap.c 
  src/jni/DropFiles_wrap.c
)

FILE(GLOB
  CONSOLE_C_SOURCES 
  src/c/InitializeConsole.c 
  src/c/GuiManagement.c 
  src/c/dropFiles.c
)

FILE(GLOB
  GIWS_WRAPPERS 
  src/jni/CallScilabBridge.giws.xml
)

ADD_LIBRARY(sciconsole-disable
  src/noconsole/noconsole.c
)

ADD_LIBRARY(sciconsole 
  ${CONSOLE_C_JNI_SOURCES}
  ${CONSOLE_CPP_JNI_SOURCES}
  ${CONSOLE_CPP_SOURCES}
  ${CONSOLE_C_SOURCES}
)

TARGET_LINK_LIBRARIES(sciconsole
  action_binding 
  shell 
  fileio 
  MALLOC 
  ${JNI_LIBRARIES}
)

IF(GUI)
  USEANT(sciconsole)
ENDIF(GUI)



#GIWS(console ${GIWS_WRAPPERS})