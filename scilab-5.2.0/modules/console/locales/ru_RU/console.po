# Russian translation for scilab
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2007-10-03 17:11+0200\n"
"PO-Revision-Date: 2009-07-01 13:52+0000\n"
"Last-Translator: kkirill <Unknown>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-12-15 16:53+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

msgid "Could not change the Console Font."
msgstr "Не удалось изменить шрифт в командном окне"

msgid "Could not change the Console Foreground."
msgstr "Не удалось изменить цвет фона в командном окне"

msgid "File or Directory"
msgstr "Файл или Каталог"

msgid "Graphics handle field"
msgstr "Поле графического дескриптора"

msgid "No help"
msgstr "Помощи нет"

msgid "Out of Screen"
msgstr "Вне экрана"

msgid "Scilab Command"
msgstr "Команда Scilab"

msgid "Scilab Function"
msgstr "Функция Scilab"

msgid "Scilab Macro"
msgstr "Макрос Scilab"

msgid "Scilab Variable"
msgstr "Переменная Scilab"
