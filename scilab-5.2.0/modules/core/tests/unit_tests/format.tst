// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

//format

fmt=format();
format('v')
sqrt(3)
format(10)
sqrt(3)
format(12,'v')
sqrt(3)
format('v',15)
sqrt(3)
format('e')
sqrt(3)
format(10)
sqrt(3)
f=format();
if or(f<>[0 10]) then pause,end
format(10,'v');
