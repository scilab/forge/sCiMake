//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA - Pierre MARECHAL <pierre.marechal@scilab.org>
//
// This file is distributed under the same license as the Scilab package.
//

//
// Sliding pendulum, curve #2
//

exec(SCI+"/modules/differential_equations/demos/dae/dae2/pendg.sci");
exec(SCI+"/modules/differential_equations/demos/dae/dae2/pendc1.sci");
exec(SCI+"/modules/differential_equations/demos/dae/dae2/demo_sliding_pendulum.sci");

demo_sliding_pendulum()
