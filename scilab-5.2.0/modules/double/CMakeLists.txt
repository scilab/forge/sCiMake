FILE(GLOB
  INC
  includes/*.h
)

#DEFINE_INCLUDES(${INC})

FILE(GLOB
  GATEWAY_C_SOURCES
  sci_gateway/c/gw_double.c
)

FILE(GLOB
  GATEWAY_FORTRAN_SOURCES
  sci_gateway/fortran/vecldiv.f 
  sci_gateway/fortran/vecmul.f 
  sci_gateway/fortran/matxpow.f 
  sci_gateway/fortran/vecimpl.f 
  sci_gateway/fortran/matldiv.f 
  sci_gateway/fortran/matsubst.f 
  sci_gateway/fortran/vecrdiv.f 
  sci_gateway/fortran/matchsgn.f 
  sci_gateway/fortran/matrdiv.f 
  sci_gateway/fortran/matrc.f 
  sci_gateway/fortran/matpow.f
  sci_gateway/fortran/matins1.f 
  sci_gateway/fortran/matins2.f 
  sci_gateway/fortran/matcmp.f 
  sci_gateway/fortran/matmult.f 
  sci_gateway/fortran/matext1.f 
  sci_gateway/fortran/matext2.f 
  sci_gateway/fortran/mattr.f 
  sci_gateway/fortran/matcc.f 
  sci_gateway/fortran/mattrc.f 
  sci_gateway/fortran/matadd.f
) 

ADD_LIBRARY(double
  ${GATEWAY_C_SOURCES}
  ${GATEWAY_FORTRAN_SOURCES}
)

TARGET_LINK_LIBRARIES(double
  boolean
  elementary_functions
  polynomials
  linear_algebra
  core
  output_stream
)