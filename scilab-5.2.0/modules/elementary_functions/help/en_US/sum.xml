<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="sum" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>sum</refname>

    <refpurpose>sum (row sum, column sum) of vector/matrix
    entries</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>y=sum(x)
y=sum(x,'r') or y=sum(x,1)

y=sum(x,'c') or y=sum(x,2)

y=sum(x,'m')</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>x</term>

        <listitem>
          <para>vector or matrix (real, complex, sparse or polynomial)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>y</term>

        <listitem>
          <para>scalar or vector</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>For a vector or a matrix <literal>x</literal>,
    <literal>y=sum(x)</literal> returns in the scalar <literal>y</literal> the
    sum of all the entries of <literal>x</literal>.</para>

    <para><literal>y=sum(x,'r')</literal> (or, equivalently,
    <literal>y=sum(x,1)</literal>) is the rowwise sum: <literal>y(j)=
    sum(x(:,j))</literal>. <literal>y</literal> is a row vector</para>

    <para><literal>y=sum(x,'c')</literal> (or, equivalently,
    <literal>y=sum(x,2)</literal>) is the columnwise sum. It returns in each
    entry of the column vector <literal>y</literal> the sum : <literal>y(i)=
    sum(x(i,:))</literal>)).</para>

    <para><literal>y=sum(x,'m')</literal> is the sum along the first non
    singleton dimension of <literal>x</literal> (for compatibility with
    Matlab).</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
A=[1,2;3,4];
trace(A)-sum(diag(A))
sum(A,'c')-A*ones(2,1)
sum(A+%i)
A=sparse(A);sum(A,'c')-A*ones(2,1)
s=poly(0,'s');
M=[s,%i+s;s^2,1];
sum(M),sum(M,2)
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="cumsum">cumsum</link></member>

      <member><link linkend="prod">prod</link></member>
    </simplelist>
  </refsection>
</refentry>
