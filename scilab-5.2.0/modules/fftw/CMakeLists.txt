FILE(GLOB
  INC
  includes/*.h
)

DEFINE_INCLUDES(${INC})

FILE(GLOB
  FFTW_C_SOURCES
  src/c/callfftw.c 
  src/c/fftwlibname.c 
  src/c/fftw_utilities.c 
  src/c/with_fftw.c
)

FILE(GLOB
  GATEWAY_C_SOURCES
  sci_gateway/c/gw_fftw.c 
  sci_gateway/c/sci_fftwlibraryisloaded.c 
  sci_gateway/c/sci_disposefftwlibrary.c 
  sci_gateway/c/sci_fftw.c 
  sci_gateway/c/sci_fftw_flags.c 
  sci_gateway/c/sci_loadfftwlibrary.c 
  sci_gateway/c/sci_get_fftw_wisdom.c 
  sci_gateway/c/sci_set_fftw_wisdom.c 
  sci_gateway/c/sci_fftw_forget_wisdom.c
) 

ADD_LIBRARY(fftw
  ${FFTW_C_SOURCES}
  ${GATEWAY_C_SOURCES}
)

TARGET_LINK_LIBRARIES(fftw
  MALLOC
  core
  dynamiclibrary
  output_stream
  elementary_functions
)