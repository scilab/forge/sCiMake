//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("misc.dem.gateway.sce");
demo_begin;
demo_message(demopath+"/misc.sce");
demo_run(demopath+"/misc.sce");
demo_end();
