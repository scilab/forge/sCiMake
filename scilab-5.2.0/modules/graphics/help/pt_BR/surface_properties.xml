<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Djalel Abdemouche
 * Copyright (C) INRIA - Fabrice Leray
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="surface_properties"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>surface_properties</refname>

    <refpurpose>Descri��o das propriedades da entidade Surface
    (superf�cie)</refpurpose>
  </refnamediv>

  <refsection>
    <title>Descri��o</title>

    <para>A entidade Surface � uma folha das hierarquia de entidades gr�ficas.
    Duas classes aparecem sob este tipo de entidade :
    <literal>Plot3d</literal> e <literal>Fac3d</literal> de acordo com a
    fun��o de esbo�o ou com o modo como os dados foram inseridos. As entidades
    <literal>Fac3d</literal> e <literal>Plo3d</literal> s�o semelhantes, mas
    <literal>Fac3d</literal> � mais completa e aceita mais op��es que
    <literal>Plot3d</literal>. Para sempre ter entidades
    <literal>Fac3d</literal> simplesmente use <literal>genfac3d</literal> para
    pr�-construir matrizes antes de usar os comandos <literal>plot3d</literal>
    ou use o comando <literal>surf</literal>.</para>

    <para>Aqui est�o as propriedades contidas em uma entidade
    superf�cie:</para>

    <variablelist>
      <varlistentry>
        <term>parent:</term>

        <listitem>
          <para>esta propriedade cont�m o manipulador da raiz. A raiz de uma
          entidade Surface pode ser <literal>"Axes"</literal> ou
          <literal>"Compound"</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>children:</term>

        <listitem>
          <para>esta propriedade cont�m um vetor com os galhos do manipulador.
          Contudo, manipuladores de superf�cie n�o possuem galhos
          correntemente. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>visible:</term>

        <listitem>
          <para>este campo cont�m o valor padr�o da propriedade
          <literal>visible</literal> para a entidade. Pode ser <literal>"on"
          </literal> ou <literal>"off"</literal> . Por padr�o, superf�cies s�o
          vis�veis , o valor da propriedade � <literal>"on"</literal>. Se for
          <literal>"off"</literal> os gr�ficos 3d n�o s�o exibidos na
          tela.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>surface_mode:</term>

        <listitem>
          <para> este campo cont�m o valor padr�o da propriedade
          <literal>surface_mode</literal> para a superf�cie. Pode ser
          <literal>"on" </literal> (superf�cie desenhada) ou
          <literal>"off"</literal> (nenhuma superf�cie desenhada). </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>foreground:</term>

        <listitem>
          <para>se <literal>color_mode &gt;= 0</literal>, cont�m o �ndice da
          cor a ser usada nas bordas. Se n�o, foreground n�o � usado. O valor
          foreground deve ser um �ndice inteiro de cor (relativo ao mapa de
          cores corrente).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>thickness:</term>

        <listitem>
          <para>este campo cont�m o valor de <literal>thickness</literal>
          (espessura) das linhas usadas para desenhar facetas ou contornos.
          Deve ser um inteiro positivo. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mark_mode:</term>

        <listitem>
          <para>este campo cont�m o valor padr�o da propriedade
          <literal>mark_mode</literal> para a superf�cie. Seu valor pode ser
          <literal>"on"</literal> (marcas desenhadas) ou
          <literal>"off"</literal> (marcas n�o desenhadas).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mark_style:</term>

        <listitem>
          <para>o valor da propriedade <literal>mark_style</literal> � usado
          para selecionar o tipo de marca usada quando a propriedade
          <literal>mark_mode</literal> � <literal>"on"</literal>. O valor deve
          ser um inteiro no intervalo [0 14] que significa: ponto, mais, cruz,
          estrela, rombo preenchido, rombo, tri�ngulo para cima, tri�ngulo
          para baixo, rombo mais, c�rculo, aster�sco, quadrado, tr�ngulo para
          direita, tri�ngulo para esquerda e pentagrama.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mark_size_unit:</term>

        <listitem>
          <para>este campo cont�m o valor padr�o da propriedade
          <literal>mark_size_unit</literal>. Se
          <literal>mark_size_unit</literal> � ajustado para
          <literal>"point"</literal>, ent�o o valor de
          <literal>mark_size</literal> � dado diretamente em pontos. Quando
          <literal>mark_size_unit</literal> � ajustado para
          <literal>"tabulated"</literal>, <literal>mark_size</literal> �
          computado de acordo com o array de tamanho de fonte: logo, seu valor
          deve ser um inteiro no intervalo [0 5] que significa 8pt, 10pt,
          12pt, 14pt, 18pt e 24pt. Note que <link
          linkend="plot3d">plot3d</link> e fun��es puras do Scilab usam o modo
          <literal>tabulated</literal> como padr�o; quando se utiliza as
          fun��es <link linkend="surf">surf</link> (ou <link
          linkend="plot">plot</link> para linhas 2d) o modo
          <literal>point</literal> � automaticamente habilitado. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mark_size:</term>

        <listitem>
          <para>a propriedade <literal>mark_size</literal> � utilizada para
          selecionar o tamanho de fonte das marcas quando a propriedade
          <literal>mark_mode</literal> est� <literal>"on"</literal>. O valor
          deve ser um inteiro entre 0 e 5 que significa 8pt, 10pt, 12pt, 14pt,
          18pt e 24pt. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mark_foreground:</term>

        <listitem>
          <para>este campo cont�m o valor padr�o da propriedade
          <literal>mark_foreground</literal> que � a cor da borda das marcas.
          Seu valor deve ser um �ndice de cor (relativo ao mapa de cores
          corrente). </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mark_background:</term>

        <listitem>
          <para>este campo cont�m o valor padr�o da propriedade
          <literal>mark_background</literal> que � a cor da face das marcas.
          Seu valor deve ser um �ndice de cor (relativo ao mapa de cores
          corrente). </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>data:</term>

        <listitem>
          <para>este campo define uma estrutura de dados
          <literal>tlist</literal> do tipo "3d" composto de �ndices de uma
          linha e uma coluna de cada elemento como as coordenadas x, y e z
          contidas respectivamente em
          <literal>data.x</literal>,<literal>data.y</literal> e
          <literal>data.z</literal>. O campo complementar
          <literal>data.color</literal> est� dispon�vel caso um vetor ou
          matriz reais de cores seja especificado. Se nenhum for,
          <literal>data.color</literal> n�o � listado. A superf�cie � pintada
          de acordo com as propriedades <literal>color_mode </literal>e
          <literal>color_flag</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>color_mode:</term>

        <listitem>
          <para>um inteiro no intervalo <literal>[-size(colormap) ;
          size(colormap)]</literal> definindo a cor da faceta quando o valor
          de <literal>color_flag</literal> � <literal>0</literal>. Como dito
          antes, se<literal> color_mode &gt; 0</literal>, as bordas s�o
          desenhadas usando a cor <literal>foreground</literal>. Se
          <literal>color_mode</literal> � ajustado para <literal>0</literal>,
          uma malha das superf�cie � desenhada: faces da frente n�o t�m cores.
          Finalmente, quando <literal>color_mode &lt; 0</literal>, as faces da
          frente s�o pintadas com a cor <literal>-color_mode</literal> mas
          nenhuma borda � exibida. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>color_flag:</term>

        <listitem>
          <para>este campo � usado para definir o algoritimo usado para
          ajustar as cores das facetas. Note que as regras em
          <literal>color_mode</literal>, <literal>foreground</literal> e
          <literal>hiddencolor</literal> ainda s�o aplicadas nesse
          caso.</para>

          <itemizedlist>
            <listitem>
              <para><literal>color_flag == 0</literal></para>

              <itemizedlist>
                <listitem>
                  <para>Todas as facetas s�o pintadas usando-se o �ndice de
                  cores e o m�todo definido por <literal>color_mode</literal>
                  (ver acima).</para>
                </listitem>
              </itemizedlist>
            </listitem>

            <listitem>
              <para><literal>color_flag == 1</literal></para>

              <itemizedlist>
                <listitem>
                  <para>Todas as facetas s�o pintadas utilizando-se um �ndice
                  de cor por faceta proporcional a <literal>z</literal>. O
                  valor m�nimo de <literal>z</literal> � pintado utilizando-se
                  o �ndice 1, enquanto o valor m�ximo de <literal>z</literal>
                  � pintado utilizando-se o �ndice mais alto. As bordas das
                  facetas pode ser adicionalmente desenhadas dependendo do
                  valor de <literal>color_mode</literal> (ver acima).</para>
                </listitem>
              </itemizedlist>
            </listitem>

            <listitem>
              <para>Os tr�s casos restantes
              (<literal>color_flag</literal><literal>== 2,3 or 4</literal>) s�
              est�o dispon�veis para a entidade <literal>Fac3d</literal> .
              Ent�o, o valor de <literal>data.color</literal> � usado para
              ajustar cores para as facetas (�ndices do mapa de cores
              corrente) se existirem. Se n�o, o <literal>color_mode</literal>
              corrente � utilizado para pintar as facetas.</para>
            </listitem>

            <listitem>
              <para><literal>color_flag == 2 ('flat' shading)</literal></para>

              <itemizedlist>
                <listitem>
                  <para>Todas as facetas s�o pintadas utilizando-se o �ndice
                  de cor dado na propriedade <literal>data.color</literal>
                  (uma cor por faceta � necess�ria). Dois s�o os casos
                  poss�veis:</para>
                </listitem>

                <listitem>
                  <para><literal>data.color</literal> cont�m um vetor
                  <literal>color</literal> vector : se
                  <literal>color(i)</literal> for positivo, fornece a cor da
                  faceta <literal>i</literal> e a borda da faceta � desenhada
                  com o estilo de linha e cor correntes. Se
                  <literal>color(i)</literal> for negativo, o a cor de
                  identificador <literal>-color(i)</literal> � usado e a borda
                  da faceta n�o � desenhada.</para>

                  <para><literal>data.color</literal> cont�m uma matriz de
                  cores de tamanho (nf,n) onde <literal>n</literal> significa
                  o n�mero de facetas e <literal>nf</literal> o n�mero de
                  pontos definindo a faceta poligonal. Para os v�rtices
                  <literal>nf</literal> definindo cada faceta, o algoritmo
                  computa o valor m�dio do �ndice de cores (da matriz de
                  �ndices de cores) : os <literal>nf</literal> v�rtices da
                  mesma faceta ter�o o mesmo valor de �ndice de cor.</para>
                </listitem>
              </itemizedlist>
            </listitem>

            <listitem>
              <para><literal>color_flag == 3 ('interpolated'
              shading)</literal></para>

              <itemizedlist>
                <listitem>
                  <para>Pinta as facetas por interpola��o das cores dos
                  v�rtices. Os �ndices das cores dos v�rtices s�o dados pela
                  propriedade <literal>data.color</literal> (uma cor por
                  v�rtice � necess�ria). Dois s�o os casos poss�veis:</para>
                </listitem>

                <listitem>
                  <para><literal>data.color</literal> cont�m um vetor
                  <literal>colors</literal> : ent�o h� poucos dados para
                  completar o modo de grada��o interpolada. De fato, uma
                  matriz de cores de tamanho (nf,n) (onde <literal>n</literal>
                  � o n�mero de facetas e <literal>nf</literal> o n�mero de
                  pontos definindo a faceta poligonal) � necess�ria para
                  realizar esta opera��o. Para cada faceta, o algoritmo copia
                  o �nico valor de �ndice de cor da faceta nos
                  <literal>nf</literal> v�rtices de �ndices de cores definindo
                  a borda da faceta.</para>

                  <para><literal>data.color</literal> cont�m uma matriz de
                  cores de tamanho (nf,n) (ver acima para defini��es de
                  <literal>nf</literal> e <literal>n</literal>), o modo de
                  grada��o interpolada pode ser completado normalmente
                  utilizando-se estes �ndices de cores.</para>
                </listitem>
              </itemizedlist>
            </listitem>

            <listitem>
              <para><literal>color_flag == 4 (Matlab-like 'flat'
              shading)</literal></para>

              <itemizedlist>
                <listitem>
                  <para>� o mesmo que <literal>color_flag==2</literal> com uma
                  pequena diferen�a quando <literal>data.color</literal> � uma
                  matriz. Todas as facetas s�o pintadas com o �ndice de cor
                  fornecido pela propriedade <literal>data.color</literal>
                  (uma cor por faceta � necess�ria). Dois casos s�o ent�o
                  poss�veis:</para>
                </listitem>

                <listitem>
                  <para><literal>data.color</literal> cont�m um vetor
                  <literal>color</literal> vector : se
                  <literal>color(i)</literal> � positivo, fornece a cor da
                  faceta <literal>i</literal> e a borda da faceta � desenhada
                  com o estilo de linha e cor correntes. Se
                  <literal>color(i)</literal> � negativo, a cor de
                  identificador <literal>-color(i)</literal> � utilizado e a
                  borda da faceta n�o � desenhada.</para>

                  <para><literal>data.color</literal> cont�m uma matriz de
                  cores de tamanho (nf,n) onde <literal>n</literal> � o n�mero
                  de facetas e <literal>nf</literal> o n�mero de pontos
                  definindo a faceta poligonal. Para os <literal>nf</literal>
                  v�rtices definindo cada faceta, o algoritimo toma a cor do
                  primeiro v�rtice definindo o ret�culo (faceta).</para>
                </listitem>
              </itemizedlist>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cdata_mapping:</term>

        <listitem>
          <para><literal>espec�fico para manipuladores Fac3d</literal>. Um
          string definindo um valor <literal>'scaled'</literal> ou
          <literal>'direct'</literal>. Se um <literal>data.color</literal> �
          definido, cada dado de �ndice de cor especifica um valor �nico para
          v�rtice. <literal>cdata_mapping</literal> determina se estes �ndices
          est�o em escala para serem mapeados linearmente no mapa de cores
          corrente (modo<literal> 'scaled'</literal>) ou aponta diretamente
          para o mapa de cores (modo <literal>'direct</literal>'). Esta
          propriedade � �til quando <literal>color_flag</literal> � igual a
          <literal>2</literal>,<literal>3</literal> ou
          <literal>4</literal>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>hiddencolor:</term>

        <listitem>
          <para>esta propriedade cont�m o �ndice de cor usado para desenhar as
          faces atr�s de uma superf�cie. Seu valor deve ser um inteiro
          positivo (�ndice de cores relativo ao mapa de cores corrente). Se
          for um inteiro negativo, a mesma cor que a face "vis�vel" � aplicada
          para a parte traseira. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>clip_state:</term>

        <listitem>
          <para>este campo cont�m o valor da propriedade
          <literal>clip_state</literal> para a superf�cie. O valor de
          clip_state pode ser:</para>

          <itemizedlist>
            <listitem>
              <para><literal>"off"</literal> significa que a superf�cie n�o �
              recortada. </para>
            </listitem>

            <listitem>
              <para><literal>"clipgrf"</literal> significa que a superf�cie �
              recortada fora da caixa dos eixos. </para>
            </listitem>

            <listitem>
              <para><literal>"on"</literal> significa que a superf�cie �
              recortada fora do ret�ngulo dado pela propriedade
              clip_box.</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>clip_box:</term>

        <listitem>
          <para>este campo determina a propriedade
          <literal>clip_box</literal>. Por padr�o seu valor � uma matriz vazia
          se a propriedade clip_state � "off". Em outros casos, o vetor
          <literal>[x,y,w,h]</literal> (ponto superior esquerdo, largura,
          altura) define as por��es da poligonal a serem exibidas, contudo o
          valor da propriedade <literal>clip_state</literal> ser� alterado.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>user_data:</term>

        <listitem>
          <para>este campo pode ser usado para armazenar qualquer vari�vel
          Scilab na estrutura de dados da superf�cie e recuper�-la.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Exemplos</title>

    <programlisting role="example"><![CDATA[ 
//criando uma figura
t=[0:0.3:2*%pi]'; z=sin(t)*cos(t');
[xx,yy,zz]=genfac3d(t,t,z); 
plot3d([xx xx],[yy yy],list([zz zz+4],[4*ones(1,400) 5*ones(1,400)])) 
h=get("hdl") //obtendo manipulador da entidade corrente (aqui, � a superf�cie) 
a=gca(); //obtendo eixos correntes
a.rotation_angles=[40,70]; 
a.grid=[1 1 1]; 
//criando grides
a.data_bounds=[-6,0,-1;6,6,5];
a.axes_visible="off"; 
//os eixos est�o escondidos a.axes_bounds=[.2 0 1 1]; 
f=get("current_figure");
//obtendo o manipulador da figura raiz
f.color_map=hotcolormap(64); 
//mudando o mapa de cores da figura 
h.color_flag=1; 
//colorindo de acordo com z 
h.color_mode=-2; 
//removendo as bordas das facetas
h.color_flag=2; 
//colorindo de acordo com as dadas cores
h.data.color=[1+modulo(1:400,64),1+modulo(1:400,64)];
//grada��o
h.color_flag=3; 

scf(2); // criando segunda janela e utilizando o comando surf
subplot(211)
surf(z,'cdata_mapping','direct','facecol','interp')

subplot(212)
surf(t,t,z,'edgeco','b','marker','d','markersiz',9,'markeredg','red','markerfac','k')
e=gce();
e.color_flag=1 // �ndice de cor proporcional � altitude (coordenada z)
e.color_flag=2; // de volta ao modo padr�o
e.color_flag= 3; // modo de grada��o interpolada (baseada na cor azul padr�o porque o campo data.color n�o est� preenchido)
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>Ver Tamb�m</title>

    <simplelist type="inline">
      <member><link linkend="set">set</link></member>

      <member><link linkend="get">get</link></member>

      <member><link linkend="delete">delete</link></member>

      <member><link linkend="plot3d">plot3d</link></member>

      <member><link linkend="plot3d1">plot3d1</link></member>

      <member><link linkend="plot3d2">plot3d2</link></member>

      <member><link linkend="surf">surf</link></member>

      <member><link
      linkend="graphics_entities">graphics_entities</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Autores</title>

    <para>Djalel ABDEMOUCHE &amp; F.Leray</para>
  </refsection>
</refentry>
