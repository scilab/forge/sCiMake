/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2004-2006 - INRIA - Fabrice Leray
 * Copyright (C) 2006 - INRIA - Allan Cornet
 * Copyright (C) 2006 - INRIA - Jean-Baptiste Silvy
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

/*------------------------------------------------------------------------*/
/* file: get_callback_property.c                                          */
/* desc : function to retrieve in Scilab the callback field of            */
/*        a handle                                                        */
/*------------------------------------------------------------------------*/

#include "getHandleProperty.h"
#include "Interaction.h"
#include "returnProperty.h"
#include "GetProperty.h"
#include "MALLOC.h"

/*------------------------------------------------------------------------*/
int get_callback_property( sciPointObj * pobj )
{
  if(sciGetEntityType(pobj) == SCI_UIMENU || sciGetEntityType(pobj) == SCI_UICONTROL)
    {
      return GetUiobjectCallback(pobj);
    }
  else
    {
			char * callBack = sciGetCallback(pobj);
			if (callBack == NULL)
			{
				return sciReturnString("");
			}
			else
			{
				return sciReturnString( sciGetCallback( pobj ) ) ;
			}
    }
}
/*------------------------------------------------------------------------*/
