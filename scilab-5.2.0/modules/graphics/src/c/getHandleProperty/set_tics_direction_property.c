/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2004-2006 - INRIA - Fabrice Leray
 * Copyright (C) 2006 - INRIA - Allan Cornet
 * Copyright (C) 2006 - INRIA - Jean-Baptiste Silvy
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

/*------------------------------------------------------------------------*/
/* file: set_tics_direction_property.c                                    */
/* desc : function to modify in Scilab the tics_direction field of        */
/*        a handle                                                        */
/*------------------------------------------------------------------------*/

#include "setHandleProperty.h"
#include "SetProperty.h"
#include "getPropertyAssignedValue.h"
#include "SetPropertyStatus.h"
#include "GetProperty.h"
#include "Scierror.h"
#include "localization.h"

/*------------------------------------------------------------------------*/
int set_tics_direction_property( sciPointObj * pobj, size_t stackPointer, int valueType, int nbRow, int nbCol )
{

  if ( !isParameterStringMatrix( valueType ) )
  {
    Scierror(999, _("Wrong type for '%s' property: String expected.\n"), "tics_direction");
    return SET_PROPERTY_ERROR ;
  }

  if ( sciGetEntityType(pobj) != SCI_AXES )
  {
    Scierror(999, _("'%s' property does not exist for this handle.\n"),"tics_direction");
    return SET_PROPERTY_ERROR ;
  }

  if ( pAXES_FEATURE (pobj)->ny == 1 )
  { 
    if( isStringParamEqual( stackPointer, "top" ) )
    {
      pAXES_FEATURE (pobj)->dir = 'u' ;
    }
    else if ( isStringParamEqual( stackPointer, "bottom" ) )
    {
      pAXES_FEATURE (pobj)->dir = 'd' ;
    }
    else
    {
      Scierror(999, _("Wrong value for '%s' property: %s or %s expected.\n"), "tics_direction", "'top'", "'bottom'");
      return SET_PROPERTY_ERROR ;
    }
    return SET_PROPERTY_SUCCEED ;
  } 
  else
  {
    if( isStringParamEqual( stackPointer, "right" ) )
    {
      pAXES_FEATURE (pobj)->dir = 'r' ;
    }
    else if ( isStringParamEqual( stackPointer, "left" ) )
    {
      pAXES_FEATURE (pobj)->dir = 'l' ;
    }
    else
    {
      Scierror(999, _("Wrong value for '%s' property: %s or %s expected.\n"), "tics_direction", "'left'", "'right'");
      return SET_PROPERTY_ERROR ;
    }
    return SET_PROPERTY_SUCCEED ;
  }
  return SET_PROPERTY_ERROR ;
}
/*------------------------------------------------------------------------*/
