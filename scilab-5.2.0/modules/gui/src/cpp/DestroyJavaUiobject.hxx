/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA - Vincent COUVERT
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
#ifndef __DESTROY_JAVA_UIOBJECT_HXX__
#define __DESTROY_JAVA_UIOBJECT_HXX__

#include "CallScilabBridge.hxx"
extern "C"
{
#include "DestroyJavaUiobject.h"
#include "GetProperty.h"
#include "getScilabJavaVM.h"
#include "localization.h"
#include "UicontrolStyleToString.h"
#include "sciprint.h"
}
#endif /* !__DESTROY_JAVA_UIOBJECT_HXX__ */
