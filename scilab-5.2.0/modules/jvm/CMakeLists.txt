IF(GUI)
  USEANT(scijvm)
ENDIF(GUI)

FILE(GLOB
  INC
  includes/*.h
)

DEFINE_INCLUDES(${INC})

INCLUDE_DIRECTORIES(
  ${JAVA_JNI_INCLUDE} 
  ${XML_FLAGS} 
)

FILE(GLOB
  GATEWAY_C_SOURCES
  sci_gateway/c/sci_system_getproperty.c 
  sci_gateway/c/sci_system_setproperty.c 
  sci_gateway/c/sci_with_embedded_jre.c 
  sci_gateway/c/sci_javaclasspath.c 
  sci_gateway/c/gw_jvm.c 
  sci_gateway/c/sci_javalibrarypath.c
)

FILE(GLOB
  JVM_C_SOURCES
  src/c/InitializeJVM.c 
  src/c/TerminateJVM.c 
  src/c/JVM_Unix.c 
  src/c/JVM.c 
  src/c/addToClasspath.c 
  src/c/loadOnUseClassPath.c 
  src/c/loadBackGroundClassPath.c 
  src/c/getClasspath.c 
  src/c/system_getproperty.c 
  src/c/system_setproperty.c 
  src/c/loadClasspath.c 
  src/c/createMainScilabObject.c 
  src/c/JVM_commons.c 
  src/c/catchIfJavaException.c 
  src/c/loadLibrarypath.c 
  src/c/getLibrarypath.c 
  src/c/addToLibrarypath.c 
  src/c/getJvmOptions.c
)

ADD_LIBRARY(jvm-disable
  src/nojvm/nojvm.c
)

ADD_LIBRARY(scijvm
  ${JVM_C_SOURCES}
  ${GATEWAY_C_SOURCES}
)

TARGET_LINK_LIBRARIES(scijvm
  MALLOC
  fileio
  core
  dynamiclibrary
  output_stream
)