/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2007 - INRIA - Jean-Baptiste Silvy
 * desc : Class containing the driver dependent routines to draw an Axes object with JoGL
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "DrawableAxesJoGL.hxx"

namespace sciGraphics
{
/*---------------------------------------------------------------------------------*/
DrawableAxesJoGL::DrawableAxesJoGL(DrawableAxes * Axes)
  : DrawableClippedObjectJoGL(Axes) 
{

}
/*---------------------------------------------------------------------------------*/
DrawableAxesJoGL::~DrawableAxesJoGL(void)
{

}
/*---------------------------------------------------------------------------------*/
DrawableAxes * DrawableAxesJoGL::getAxesDrawer( void )
{
  return dynamic_cast<DrawableAxes *>(getDrawer());
}
/*---------------------------------------------------------------------------------*/
}
