/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2007 - INRIA - Jean-Baptiste Silvy
 * desc : Class specialized in drawing triangle left marks  
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */


package org.scilab.modules.renderer.utils.MarkDrawing;

import javax.media.opengl.GL;

/**
 * Class specialized in drawing triangle left marks
 * @author Jean-Baptiste Silvy
 */
public class TriangleLeftMarkDrawer extends TriangleMarkDrawer {

	/** angle between up triangle and down triangle */
	public static final double ROTATION_ANGLE = 90.0;
	
	/**
	 * Default constructor
	 */
	public TriangleLeftMarkDrawer() {
		super();
	}
	
	/**
	 * Draw a, equilateral triangle pointing left
	 * @param gl OpenGL pipeline to use
	 * @param backColor RGB color of mark background
	 * @param frontColor RGB color of mark foreground
	 */
	public void drawMark(GL gl, double[] backColor, double[] frontColor) {
		gl.glRotated(ROTATION_ANGLE, 0.0, 0.0, 1.0);
		super.drawMark(gl, backColor, frontColor);

	}

}
