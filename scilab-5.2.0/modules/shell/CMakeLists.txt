FILE(GLOB
  INC
  includes/*.h
)

DEFINE_INCLUDES(${INC})

INCLUDE_DIRECTORIES(
  ${JAVA_JNI_INCLUDE}
)



FILE(GLOB
  SHELL_C_SOURCES
  src/c/clc.c  
  src/c/tohome.c  
  src/c/more.c  
  src/c/scilines.c  
  src/c/GetCommandLine.c  
  src/c/others/zzledt.c  
  src/c/others/gotoxy_nw.c  
  src/c/others/clrscr_nw.c  
  src/c/InitializeShell.c  
  src/c/TerminateShell.c  
  src/c/prompt.c  
  src/c/promptecho.c
)

FILE(GLOB
  GATEWAY_C_SOURCES
  sci_gateway/c/gw_shell.c  
  sci_gateway/c/sci_clc.c  
  sci_gateway/c/sci_tohome.c  
  sci_gateway/c/sci_lines.c  
  sci_gateway/c/sci_prompt.c  
  sci_gateway/c/sci_iswaitingforinput.c
)

ADD_LIBRARY(shell
  ${SHELL_C_SOURCES}
  ${GATEWAY_C_SOURCES}
)

TARGET_LINK_LIBRARIES(shell 
  action_binding 
  history_manager 
  core 
  MALLOC 
  output_stream
  completion
)

IF(GUI)
  USEANT(shell)
ENDIF(GUI)
