/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - 2007 - Vincent COUVERT
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/ 
#ifndef __INTIALIZESHELL_H__
#define __INTIALIZESHELL_H__
#include "dynlib_shell.h"
#include "BOOL.h"

SHELL_IMPEXP BOOL InitializeShell(void);

#endif /* __INTIALIZESHELL_H__ */
/*--------------------------------------------------------------------------*/ 
