<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="erfinv">
  <info>
    <pubdate>$LastChangedDate: 2008-04-05 20:49:31 +0200 (sam, 05 avr 2008) $</pubdate>
  </info>
  <refnamediv>
    <refname>erfinv</refname>
    <refpurpose>  The inverse of the error function.  </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>y = erfinv(x)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>x</term>
        <listitem>
          <para>real vector or matrix</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>real vector or matrix (of same size than x)</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>erfinv</literal> computes the inverse of the error function
<literal>erf</literal>.
<literal>x = erfinv(y)</literal> satisfies <literal>y = erf(x)</literal>,
        -1 &lt;= <literal>y</literal> &lt; 1, -inf &lt;= <literal>x</literal> &lt;= inf.

</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
x=linspace(-0.99,0.99,100);
y=erfinv(x);
plot2d(x,y)
norm(x-erf(y),'inf')
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="erf">erfc</link>
      </member>
      <member>
        <link linkend="cdfnor">cdfnor</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>References</title>
    <para>Milton Abramowitz and Irene A. Stegun, eds. Handbook of Mathematical Functions with Formulas, Graphs, and Mathematical Tables. New York: Dover, 1972.</para>
  </refsection>
</refentry>
