/*------------------------------------------------------------------------*/
/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

/*------------------------------------------------------------------------*/
#ifndef __GET_LENGTH_H__
#define __GET_LENGTH_H__

/**
* Get_length to get the length of output string matrix
* @param[in] Input_String_One The first input string
* @param[in] Input_String_Two The second input string
* @param[in] Number_Input_One the number of input string matrix
* @return  the length of the output string matrix
*/

int Get_length(char **Input_String_One,char **Input_String_Two,int Number_Input_One);

#endif /* __GET_LENGTH_H__ */
/*------------------------------------------------------------------------*/
