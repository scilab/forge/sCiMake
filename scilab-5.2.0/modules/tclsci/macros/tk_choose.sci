// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2006 - S. Mottelet
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function _result=tk_choose(_items,_title,_button)

// Use of a namespace makes things easier (no global variables to pass
// arguments from the Scilab level).


if argn(2)<=1
    _title="";
end
if argn(2)<=2
  _button=gettext('Cancel');
end


// Create the namespace

TCL_EvalStr("namespace eval tkChoose {}")

// Set parameter arrays in the Tcl world

TCL_SetVar("tkChoose::title",_title(:));
TCL_SetVar("tkChoose::items",_items(:));
TCL_SetVar("tkChoose::button",_button(:));

// Finally launch the Tcl script

TCL_EvalFile(SCI+"/modules/tclsci/tcl/utils/tk_choose.tcl")

// Event loop at the Scilab level

while ~TCL_ExistVar("tkChoose::result")
  // wait for response
  sleep(1);
end

_result=evstr(TCL_GetVar("tkChoose::result"))+1;

// Destroy the toplevel tkChoose widget

TCL_EvalStr("catch {destroy $tkChoose::t}")

// Yank the namespace (all variables therein are also
// destroyed)

TCL_EvalStr("namespace delete tkChoose")

endfunction

