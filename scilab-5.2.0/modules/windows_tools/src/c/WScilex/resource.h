//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WScilex.rc
//
// Icon with lowest ID value placed first to ensure application icon
// remains consistent on all systems.
#define IDI_ICON_SCILAB                 101
#define IDI_ICON_BIN                    102
#define IDI_ICON_COS                    103
#define IDI_ICON_COSF                   104
#define IDI_ICON_DEM                    105
#define IDI_ICON_GRAPH                  106
#define IDI_ICON_SAV                    107
#define IDI_ICON_SCE                    108
#define IDI_ICON_SCI                    109
#define IDI_ICON_TEST                   110

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
